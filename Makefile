.PHONY: build
build:
	# $(MAKE) -C cmd/loggen build  
	docker-compose up --build

#==============================================================================

.PHONY: remove-logs
remove-logs: 
	awslocal s3 rm s3://bucket/ --recursive 

#==============================================================================
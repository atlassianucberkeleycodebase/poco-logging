package logs

import (
	"fmt"

	"github.com/alecthomas/kingpin"
)

func Mount(app *kingpin.Application) {
	cmd := app.Command("logs", "Do some poco log related things")
	cmd.Action(func(ctx *kingpin.ParseContext) error {
		fmt.Println("I did the thing")
		return nil
	})
}

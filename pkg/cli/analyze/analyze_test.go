package analyze

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/alecthomas/kingpin"
)

// Should common given inputs against policy and query correctly
func TestEvaluate(t *testing.T) {
	res, err := Execute("../../../examples/policies/service/service.rego",
		"../../../examples/policies/service/data.json",
		"data.platform.test_combined_output with input as data.platform.test.methods[x]",
		`{"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "expected"}}`)
	if err != nil {
		t.Error(err)
	}

	if len(res) != 3 {
		t.Errorf("number of tests incorrect, expected %d, actual %d", 3, len(res))
	}
	for _, result := range res {
		if result.Expressions[0].Value != true {
			t.Errorf("test failed with binding %s", result.Bindings["x"])
		}
	}
}

func TestEvaluateInputFile(t *testing.T) {
	content := []byte(`{"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "expected"}}`)
	tmpfile, err := ioutil.TempFile("", "example")
	if _, err := tmpfile.Write(content); err != nil {
		t.Error(err)
	}
	if err != nil {
		t.Error(err)
	}
	res, err := Execute("../../../examples/policies/service/service.rego",
		"../../../examples/policies/service/data.json",
		"data.platform.test_combined_output with input as data.platform.test.methods[x]",
		tmpfile.Name())
	defer os.Remove(tmpfile.Name())
	if err != nil {
		t.Error(err)
	}
	if len(res) != 3 {
		t.Errorf("number of tests incorrect, expected %d, actual %d", 3, len(res))
	}
	for _, result := range res {
		if result.Expressions[0].Value != true {
			t.Errorf("test failed with binding %s", result.Bindings["x"])
		}
	}
}

//should make correct decision on inputstring
func TestCheckInputString(t *testing.T) {
	res, err := CheckInput(`{"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "expected"}}`, "")
	if err != nil {
		t.Error(err)
	}
	if res == "" {
		t.Error(err)
	}
}

//should make correct decision on inputstring
func TestCheckInputStringFile(t *testing.T) {
	res, err := CheckInput(`{"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "expected"}}`, "../../../examples/policies/service/service.rego")
	if err != nil {
		return
	}
	if res == "" {
		t.Error(err)
	}

}

//should make correct decision on inputfile
func TestCheckFile(t *testing.T) {
	res, err := CheckInput("", "../../../examples/policies/service/service.rego")
	if err != nil {
		t.Error(err)
	}
	if res == "" {
		t.Error(err)
	}
}

// Should accept args properly
func TestCLI(t *testing.T) {
	app := kingpin.New("analyzeTest", "Mock kingpin client for testing")
	Mount(app)
	args := []string{
		"analyze",
		"--policy", "../../../examples/policies/service/service.rego",
		"--data", "../../../examples/policies/service/data.json",
		"--query", "data.platform.test_combined_output with input as data.platform.test.methods[x]",
		"--input", `{"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "expected"}}`}
	_, err := app.Parse(args)
	if err != nil {
		t.Error(err)
	}
}

// Should accept args properly
func TestCLIInputFile(t *testing.T) {
	app := kingpin.New("analyzeTest", "Mock kingpin client for testing")
	Mount(app)

	content := []byte(`{"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "expected"}}`)
	tmpfile, err := ioutil.TempFile("", "example")
	if _, err := tmpfile.Write(content); err != nil {
		t.Error(err)
	}
	if err != nil {
		t.Error(err)
	}

	args := []string{
		"analyze",
		"--policy", "../../../examples/policies/service/service.rego",
		"--data", "../../../examples/policies/service/data.json",
		"--query", "data.platform.test_combined_output with input as data.platform.test.methods[x]",
		"--input", `{"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "expected"}}`}

	_, err = app.Parse(args)
	defer os.Remove(tmpfile.Name())
	if err != nil {
		t.Error(err)
	}
}

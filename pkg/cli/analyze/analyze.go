package analyze

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"github.com/open-policy-agent/opa/rego"

	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/httpserver"
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocorego"
	"github.com/alecthomas/kingpin"
)

var (
	policyFile  *string
	dataFile    *string
	queryString *string
	inputString *string
	inputFile   *string
)

// Mount adds the analyze subcommand to the kingpin CLI app
func Mount(app *kingpin.Application) {
	cmd := app.Command("analyze", "Ingest user-supplied OPA policy for analysis")
	policyFile = cmd.Flag("policy", "Path to user policy (package service)").Required().String()
	dataFile = cmd.Flag("data", "Path to data.json (user defined auth rules)").String()
	queryString = cmd.Flag("query", "Query to run against OPA").Required().String()
	inputString = cmd.Flag("input", "Input to be used by user").String()
	inputFile = cmd.Flag("inputFile", "Path to the user input").String()
	cmd.Action(func(ctx *kingpin.ParseContext) error {
		input, err := CheckInput(*inputString, *inputFile)
		if err != nil {
			return err
		}
		results, err := Execute(*policyFile, *dataFile, *queryString, input)
		if err != nil {
			return err
		}
		resultJSON, err := json.MarshalIndent(results, "", "\t")
		if err != nil {
			return err
		}
		fmt.Printf("Results: \n%s\n", string(resultJSON[:]))
		return err
	})
}

// CheckInput ensures input is in correct format for execution
func CheckInput(inputString string, inputFile string) (input string, err error) {
	if inputString == "" && inputFile != "" {
		return pocorego.ReadFile(inputFile)
	}
	if inputString != "" && inputFile == "" {
		return inputString, nil
	}
	if inputString != "" && inputFile != "" {
		return "", errors.New("Only want input from command line or file")
	}
	return
}

// Execute reads the contents of files at policyPath and dataPath and
// evaluates the inputs and queryString against the supplied policy
func Execute(policyPath string, dataPath string, queryString string, input string) (results rego.ResultSet, err error) {
	if dataPath == "" {
		isDir, err := isDirectory(policyPath)
		if err != nil {
			return nil, err
		}
		if isDir {
			policy, err := getFilesFromExt(policyPath, ".rego")
			if err != nil {
				return nil, err
			} else if len(policy) > 1 {
				return nil, errors.New("more than one .rego file found")
			}

			data, err := getFilesFromExt(policyPath, ".json")
			if err != nil {
				return nil, err
			} else if len(data) > 1 {
				return nil, errors.New("more than one .json file found")
			}

			dataPath = filepath.Join(policyPath, data[0])
			policyPath = filepath.Join(policyPath, policy[0])
		}
	}

	userPolicy, err := pocorego.ReadFile(policyPath)
	if err != nil {
		return
	}
	userData, err := pocorego.ReadFile(dataPath)
	if err != nil {
		return
	}
	inputMap := make(map[string]interface{})
	if fileExists(input) {
		input, err = pocorego.ReadFile(input)
		if err != nil {
			return
		}
	}
	err = json.Unmarshal([]byte(input), &inputMap)
	if err != nil {
		return
	}
	request := httpserver.AnalyzeRequest{
		UserService: pocorego.UserService{
			UserPolicy: userPolicy,
			UserData:   userData,
		},
		Input: inputMap,
		Query: queryString,
	}

	req, err := json.Marshal(request)
	if err != nil {
		return
	}
	reqBody := bytes.NewBuffer(req)
	resp, err := http.Post(os.Getenv("POCO_ENDPOINT")+"/analyze", "application/json", reqBody)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	var resultSet rego.ResultSet
	err = json.Unmarshal(body, &resultSet)
	if err != nil {
		return
	}

	return resultSet, err
}

// fileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func isDirectory(path string) (bool, error) {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false, err
	}
	return fileInfo.IsDir(), err
}

func getFilesFromExt(path string, ext string) ([]string, error) {
	var result []string

	d, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer d.Close()

	files, err := d.Readdir(-1)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		if file.Mode().IsRegular() {
			if filepath.Ext(file.Name()) == ext {
				result = append(result, file.Name())
			}
		}
	}

	return result, err
}

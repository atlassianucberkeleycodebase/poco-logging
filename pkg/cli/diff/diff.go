package diff

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/httpserver"
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocorego"
	"github.com/alecthomas/kingpin"
	"github.com/jedib0t/go-pretty/table"
)

var (
	policyFile  *string
	dataFile    *string
	serviceName *string
	offset      *int
	limit       *int
)

// Mount adds the diff subcommand to the kingpin CLI app
func Mount(app *kingpin.Application) {
	cmd := app.Command("diff", "Ingest user-supplied OPA policy for finding diffs")
	policyFile = cmd.Flag("policy", "Path to user policy (package service)").Required().String()
	dataFile = cmd.Flag("data", "Path to data.json (user defined auth rules)").String()
	serviceName = cmd.Flag("service", "Name of service to run diffs against").Required().String()
	offset = cmd.Flag("offset", "Amount of logs to offset by before displaying results").Default("0").Int()
	limit = cmd.Flag("limit", "Maximum amount of logs to display").Default("1").Int()
	cmd.Action(func(ctx *kingpin.ParseContext) error {
		results, err := Execute(*policyFile, *dataFile, *serviceName, *offset, *limit)
		if err != nil {
			return err
		}
		outputTable(results)
		return err
	})
}

// Execute reads the contents of files at policyPath and dataPath and
// runs past logs against the supplied policy
func Execute(policyPath string, dataPath string, serviceName string, offset int, limit int) (result []httpserver.TopLayer, err error) {
	if offset < 0 {
		return nil, errors.New("invalid offset")
	} else if limit < 1 {
		return nil, errors.New("invalid limit")
	}

	if dataPath == "" {
		isDir, err := isDirectory(policyPath)
		if err != nil {
			return nil, err
		}
		if isDir {
			policy, err := getFilesFromExt(policyPath, ".rego")
			if err != nil {
				return nil, err
			} else if len(policy) > 1 {
				return nil, errors.New("more than one .rego file found")
			}

			data, err := getFilesFromExt(policyPath, ".json")
			if err != nil {
				return nil, err
			} else if len(data) > 1 {
				return nil, errors.New("more than one .json file found")
			}

			dataPath = filepath.Join(policyPath, data[0])
			policyPath = filepath.Join(policyPath, policy[0])
		}
	}

	userPolicy, err := pocorego.ReadFile(policyPath)
	if err != nil {
		return nil, err
	}
	userData, err := pocorego.ReadFile(dataPath)
	if err != nil {
		return nil, err
	}

	req := httpserver.DiffRequest{
		UserService: pocorego.UserService{
			UserPolicy: userPolicy,
			UserData:   userData,
		},
		ServiceName: serviceName,
	}
	reqJSON, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	reqBody := bytes.NewBuffer(reqJSON)

	resp, err := http.Post(os.Getenv("POCO_ENDPOINT")+"/diff", "application/json", reqBody)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var resultMap []httpserver.TopLayer
	err = json.Unmarshal(body, &resultMap)
	if err != nil {
		return nil, err
	}
	if len(resultMap) < offset {
		return nil, errors.New("invalid offset")
	}
	resultMap = resultMap[offset:]
	if len(resultMap) > limit {
		resultMap = resultMap[:limit]
	}

	return resultMap, err
}

func outputTable(data []httpserver.TopLayer) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"HTTP", "Endpoint", "", ""})
	for _, topLayer := range data {
		t.AppendRow(table.Row{topLayer.Content.HttpMethod, topLayer.Content.Endpoint, "", "", ""})
		if !topLayer.HasChildren {
			continue
		}
		for _, child := range topLayer.Children {
			value := child.Content.Value
			if child.Content.HasChanged {
				value += " (CHANGED)"
			}
			t.AppendRow(table.Row{"", child.Content.DecisionID, child.Content.Mechanism, child.Content.Principals, value})
		}
	}
	t.Render()
}

func isDirectory(path string) (bool, error) {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false, err
	}
	return fileInfo.IsDir(), err
}

func getFilesFromExt(path string, ext string) ([]string, error) {
	var result []string

	d, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer d.Close()

	files, err := d.Readdir(-1)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		if file.Mode().IsRegular() {
			if filepath.Ext(file.Name()) == ext {
				result = append(result, file.Name())
			}
		}
	}

	return result, err
}

package diff

import (
	"testing"

	"github.com/alecthomas/kingpin"
)

// Should find diffs against policy correctly
func TestEvaluate(t *testing.T) {
	_, err := Execute("../../../examples/policies/service/service.rego",
		"../../../examples/policies/service/data.json",
		"service-a", 0, 1,
	)
	if err != nil {
		t.Error(err)
	}
}

// Should accept args properly
func TestCLI(t *testing.T) {
	app := kingpin.New("diffTest", "Mock kingpin client for testing")
	Mount(app)
	args := []string{
		"diff",
		"--policy", "../../../examples/policies/service",
		"--service", "service-a",
	}
	_, err := app.Parse(args)
	if err != nil {
		t.Error(err)
	}
}

package services

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/httpserver"
	"github.com/alecthomas/kingpin"
)

// Mount adds the services subcommand to the kingpin CLI app
func Mount(app *kingpin.Application) {
	cmd := app.Command("services", "Returns the list of service names that are available for diff")
	cmd.Action(func(ctx *kingpin.ParseContext) error {
		results, err := Execute()
		if err != nil {
			return err
		}
		fmt.Println("Services:")
		for _, service := range results {
			fmt.Println(service)
		}
		return err
	})
}

// Execute retrieves result from service endpoint
func Execute() (result httpserver.Services, err error) {
	resp, err := http.Get(os.Getenv("POCO_ENDPOINT") + "/services")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var services httpserver.Services
	err = json.Unmarshal(body, &services)
	if err != nil {
		return nil, err
	}

	return services, err
}

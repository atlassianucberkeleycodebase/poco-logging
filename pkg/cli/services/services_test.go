package services

import (
	"testing"

	"github.com/alecthomas/kingpin"
)

// Should retrieve service names correctly
func TestEvaluate(t *testing.T) {
	_, err := Execute()
	if err != nil {
		t.Error(err)
	}
}

// Should accept args properly
func TestCLI(t *testing.T) {
	app := kingpin.New("servicesTest", "Mock kingpin client for testing")
	Mount(app)
	args := []string{"services"}
	_, err := app.Parse(args)
	if err != nil {
		t.Error(err)
	}
}

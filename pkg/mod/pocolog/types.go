package pocolog

import (
	"errors"
	"github.com/open-policy-agent/opa/rego"
	"reflect"
	"strings"
)

type PocoLog struct {
	PolicyControl PolicyControl `json:"policyControl"`
	Timestamp     string        `json:"timestamp"`
	Micros        MicrosInfo    `json:"m"`
	PocoInput     *PocoInput    `json:"-"`
}

type MicrosInfo struct {
	Version    string `json:"sv"`
	Service    string `json:"si"`
	Deployment string `json:"di"`
}

type PolicyControl struct {
	DecisionID string                 `json:"decision_id"`
	Input      map[string]interface{} `json:"input"`
	Query      string                 `json:"query"`
	Result     rego.ResultSet         `json:"result"`
}

type PocoInput struct {
	Path       string            `json:"path"`
	Method     string            `json:"method"`
	Principals string            `json:"principals"`
	Mechanism  string            `json:"mechanism"`
	Headers    map[string]string `json:"headers"`
}

// Input tries to convert the unstructured input map into the schema defined by PocoInput
func (pocoLog *PocoLog) Input() (*PocoInput, error) {
	if pocoLog.PocoInput != nil {
		return pocoLog.PocoInput, nil
	}

	pocoLog.PocoInput = &PocoInput{}
	pocoLog.PocoInput.Headers = make(map[string]string)

	// Extracting fields from input
	if pathField, ok := pocoLog.PolicyControl.Input["path"]; ok {
		pocoLog.PocoInput.Path = pathField.(string)
	} else {
		return nil, errors.New(`missing field "path" from pocoinput`)
	}
	if methodField, ok := pocoLog.PolicyControl.Input["method"]; ok {
		pocoLog.PocoInput.Method = methodField.(string)
	} else {
		return nil, errors.New(`missing field "method" from pocoinput`)
	}
	if principalsField, ok := pocoLog.PolicyControl.Input["principals"]; ok {
		principalsSlice := make([]string, 0, len(principalsField.([]interface{})))
		for _, v := range principalsField.([]interface{}) {
			principalsSlice = append(principalsSlice, v.(string))
		}
		pocoLog.PocoInput.Principals = strings.Join(principalsSlice, ",")
	} else {
		return nil, errors.New(`missing field "principals" from pocoinput`)
	}
	if mechanismField, ok := pocoLog.PolicyControl.Input["mechanism"]; ok {
		pocoLog.PocoInput.Mechanism = mechanismField.(string)
	} else {
		return nil, errors.New(`missing field "mechanism" from pocoinput`)
	}
	if headers, ok := pocoLog.PolicyControl.Input["headers"]; ok && headers != nil {
		for k, v := range headers.(map[string]interface{}) {
			pocoLog.PocoInput.Headers[k] = v.(string)
		}
	}
	return pocoLog.PocoInput, nil
}

// TODO: Stretch goal - reporting line numbers in .json or .rego that caused diffs
func (pocoLog *PocoLog) Difference(result rego.ResultSet) (res bool, err error) {
	// To compare results, we cannot only use reflect.DeepEqual because json.Unmarshal represents
	// empty fields as nil while OPA represents them as an empty struct
	for idx, res1 := range pocoLog.PolicyControl.Result {
		res2 := result[idx]
		if res1.Bindings != nil && len(res1.Bindings) != 0 {
			if res2.Bindings == nil || len(res2.Bindings) != len(res1.Bindings) {
				return false, nil
			}
			for bIdx, bind1 := range res1.Bindings {
				bind2 := res2.Bindings[bIdx]
				if !reflect.DeepEqual(bind1, bind2) {
					return false, nil
				}
			}
		}

		if res1.Expressions != nil && len(res1.Expressions) != 0 {
			if res2.Expressions == nil || len(res2.Expressions) != len(res1.Expressions) {
				return false, nil
			}
			for eIdx, expr1 := range res1.Expressions {
				expr2 := res2.Expressions[eIdx]
				if !reflect.DeepEqual(*expr1, *expr2) {
					return false, nil
				}
			}
		}
	}
	return true, nil
}

func (pocoLog *PocoLog) GetOptions() (options []func(*rego.Rego), err error) {
	options = make([]func(*rego.Rego), 1)
	options[0] = rego.Input(pocoLog.PolicyControl.Input)
	return options, nil
}

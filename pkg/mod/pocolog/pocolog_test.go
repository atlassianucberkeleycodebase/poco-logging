package pocolog

import (
	"fmt"
	"strconv"
	"testing"
)

func TestReadLog(t *testing.T) {
	logs, err := GetLogsByIndex(0, 1)
	if err != nil {
		t.Error(err)
	}

	logCount := len(logs)
	fmt.Println("Retrieved " + strconv.Itoa(logCount) + " logs.")

	nextLogs, err := GetLogsByIndex(1, 2)
	if err != nil {
		t.Error(err)
	}
	for _, log := range nextLogs {
		logs = append(logs, log)
	}

	fmt.Println("Retrieved " + strconv.Itoa(len(logs)-logCount) + " more logs.")

	fmt.Print("Output of the first log found in service `test`:")
	fmt.Println(logs[0])
}

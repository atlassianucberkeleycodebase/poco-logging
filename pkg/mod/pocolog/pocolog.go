package pocolog

import (
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/s3"
	"bytes"
	"compress/gzip"
	"encoding/json"
	"io"
	"time"
)

var (
	S3client s3.S3
)

func init() {
	client, err := s3.NewS3BucketClient()
	for err != nil {
		time.Sleep(time.Second * 10)
		client, err = s3.NewS3BucketClient()
	}
	S3client = client
}

// GetLogsByKey retrieves logs corresponding to a given key from the S3 bucket
func GetLogsByKey(key string) (logs []PocoLog, err error) {
	objectsMeta, err := S3client.ListObjects(key, 1000, 0)
	if err != nil {
		return
	}
	logs = make([]PocoLog, 0, len(objectsMeta))
	for _, meta := range objectsMeta {
		obj, err := S3client.GetObject(meta.Key)
		if err != nil {
			return nil, err
		}
		testLog, err := ParseLogBytes(obj.Value)
		if err != nil {
			return nil, err
		}
		logs = append(logs, testLog...)
	}
	return
}

// GetLogsByKey retrieves logs within a certain index from the S3 bucket
func GetLogsByIndex(start int, end int) (logs []PocoLog, err error) {
	objectsMeta, err := S3client.ListObjects("", end-start, start)
	if err != nil {
		return
	}
	logs = make([]PocoLog, 0, len(objectsMeta))
	for _, meta := range objectsMeta {
		obj, err := S3client.GetObject(meta.Key)
		if err != nil {
			return nil, err
		}
		testLog, err := ParseLogBytes(obj.Value)
		if err != nil {
			return nil, err
		}
		logs = append(logs, testLog...)
	}
	return
}

// ParseLogBytes unzips and parses S3 data into PocoLog structs
func ParseLogBytes(logBytesCompressed []byte) (logs []PocoLog, err error) {
	logBytes, err := gzip.NewReader(bytes.NewBuffer(logBytesCompressed))
	if err != nil {
		return nil, err
	}

	var objects = make([]PocoLog, 0)
	decoder := json.NewDecoder(logBytes)
	for {
		var data PocoLog

		err := decoder.Decode(&data)
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		objects = append(objects, data)
	}

	return objects, nil
}

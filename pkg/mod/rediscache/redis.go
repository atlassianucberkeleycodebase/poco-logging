package rediscache

import (
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocolog"
	"encoding/json"
	"github.com/gomodule/redigo/redis"
	"github.com/google/uuid"
	"log"
	"time"
)

type Redis interface {
	RawUrl() string
	Client() redis.Conn

	AddNewLogs(map[string][]pocolog.PocoLog) (map[string]int, error)
	GetLogs(service string) ([]pocolog.PocoLog, error)
	ListServices() ([]string, error)
	GetExpiredLogKeys() (map[string][]string, error)
	DeleteLogs(map[string][]string) (map[string][]pocolog.PocoLog, error)
}

type DefaultRedisClient struct {
	// See redigo.DialURL or https://www.iana.org/assignments/uri-schemes/prov/redis
	rawurl  string
	options []redis.DialOption
	client  redis.Conn
	expiry  time.Duration
}

func NewRedisClient(rawurl string, expiry time.Duration, options ...redis.DialOption) *DefaultRedisClient {
	c := &DefaultRedisClient{
		rawurl:  rawurl,
		options: options,
		expiry:  expiry,
	}
	c.client = c.Client()
	return c
}

func (c *DefaultRedisClient) RawUrl() string {
	return c.rawurl
}

// Blocking
func (c *DefaultRedisClient) Client() redis.Conn {
	for {
		client, err := redis.DialURL(c.rawurl, c.options...)
		if err != nil {
			log.Printf("Failed to connect to redis: %v", err)
			time.Sleep(10 * time.Second)
			continue
		} else {
			return client
		}
	}
}

// AddNewLogs adds logs split by service into corresponding redis hashmap and updates the corresponding sorted set
func (c *DefaultRedisClient) AddNewLogs(splitLogs map[string][]pocolog.PocoLog) (updates map[string]int, err error) {
	servicesArgs := redis.Args{}
	servicesArgs = servicesArgs.Add("services")
	for service, testLogs := range splitLogs {
		// Maintaining the set of services
		servicesArgs = servicesArgs.Add(service)
		// Arguments for HSET are KEY field1 value1 [field2 value2...]
		hashmapArgs := redis.Args{}
		hashmapArgs = hashmapArgs.Add(service + ":logs")
		// Arguments for ZADD are KEY score1 member1 [score2 member2...]
		sortedSetArgs := redis.Args{}
		sortedSetArgs = sortedSetArgs.Add(service + ":hmkeys")

		for _, testLog := range testLogs {
			hmkey := uuid.New().String()
			timestamp, err := time.Parse(time.RFC3339Nano, testLog.Timestamp)
			if err != nil {
				return nil, err
			}
			testLogBytes, err := json.Marshal(testLog)
			if err != nil {
				return nil, err
			}
			// key -> log
			hashmapArgs = hashmapArgs.Add(hmkey)
			hashmapArgs = hashmapArgs.Add(testLogBytes)
			// score, key
			sortedSetArgs = sortedSetArgs.Add(timestamp.Unix())
			sortedSetArgs = sortedSetArgs.Add(hmkey)
		}

		c.client.Send("HSET", hashmapArgs...)
		c.client.Send("ZADD", sortedSetArgs...)
	}
	c.client.Send("SADD", servicesArgs...)
	c.client.Flush()

	// Getting replies from pipelined commands
	updates = make(map[string]int)
	for service, _ := range splitLogs {
		hmreply, err := redis.Int(c.client.Receive()) // reply from HSET
		if err != nil {
			return nil, err
		}
		zaddreply, err := redis.Int(c.client.Receive()) // reply from ZADD
		if err != nil {
			return nil, err
		}
		if zaddreply != hmreply {
			log.Printf("Unexpected difference between replies of HSET (%d) and ZADD (%d) in rediscache.AddNewLogs", hmreply, zaddreply)
		}
		updates[service] = hmreply
	}
	// Reply from SADD
	_, _ = c.client.Receive()
	return updates, nil
}

// GetLogs returns all the logs in the redis hashmap for a given service
func (c *DefaultRedisClient) GetLogs(service string) ([]pocolog.PocoLog, error) {
	testLogsBytes, err := redis.ByteSlices(c.client.Do("HVALS", service+":logs"))
	if err != nil {
		return nil, err
	}
	testLogs := make([]pocolog.PocoLog, len(testLogsBytes), len(testLogsBytes))
	for idx, testLogBytes := range testLogsBytes {
		testLog := pocolog.PocoLog{}
		err := json.Unmarshal(testLogBytes, &testLog)
		if err != nil {
			return nil, err
		}
		testLogs[idx] = testLog
	}
	return testLogs, nil
}

// ListServices lists all the services that have logs in redis
func (c *DefaultRedisClient) ListServices() ([]string, error) {
	return redis.Strings(c.client.Do("SMEMBERS", "services"))
}

// GetExpiredLogKeys returns, for each service, the hashmap keys corresponding to the expired logs
// as determined by the client's expiry
func (c *DefaultRedisClient) GetExpiredLogKeys() (map[string][]string, error) {
	services, err := c.ListServices()
	if err != nil {
		return nil, err
	}
	maxLim := time.Now().Add(-c.expiry).Unix()
	for _, service := range services {
		c.client.Send("ZRANGEBYSCORE", service+":hmkeys", "-inf", maxLim)
	}
	c.client.Flush()
	res := make(map[string][]string)
	for _, service := range services {
		expired, err := redis.Strings(c.client.Receive())
		if err != nil {
			return nil, err
		}
		res[service] = expired
	}
	return res, nil
}

// DeleteLogs takes in a map (serviceName -> []logKey) and removes the corresponding logs from the cache
func (c *DefaultRedisClient) DeleteLogs(expired map[string][]string) (map[string][]pocolog.PocoLog, error) {
	// Popping logs and keys from hashmap and returning them
	updates := make(map[string][]pocolog.PocoLog)
	for service, keys := range expired {
		if len(keys) > 0 {
			hmkeysArgs := redis.Args{}.Add(service + ":hmkeys").AddFlat(keys)
			logsArgs := redis.Args{}.Add(service + ":logs").AddFlat(keys)
			c.client.Send("HMGET", logsArgs...)
			c.client.Send("ZREM", hmkeysArgs...)
			c.client.Send("HDEL", logsArgs...)
			updates[service] = make([]pocolog.PocoLog, 0, len(keys))
		}
	}
	c.client.Flush()
	// Getting replies from pipelined commands
	for service, _ := range expired {
		if _, ok := updates[service]; ok {
			hmgetReply, err := redis.ByteSlices(c.client.Receive()) // reply from HMGET\
			if err != nil {
				return nil, err
			}
			for _, testLogBytes := range hmgetReply {
				testLog := pocolog.PocoLog{}
				err = json.Unmarshal(testLogBytes, &testLog)
				if err != nil {
					return nil, err
				}
				updates[service] = append(updates[service], testLog)
			}
			_, _ = redis.Int(c.client.Receive()) // reply from ZREM
			_, _ = redis.Int(c.client.Receive()) // reply from HDEL
		}
	}
	return updates, nil
}

package rediscache

import (
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocolog"
	"net/http"
	"os"
	"reflect"
	"sort"
	"testing"
	"time"
)

var (
	client *DefaultRedisClient
	inputs = map[string][]pocolog.PocoLog{
		"barservice": {
			{
				PolicyControl: pocolog.PolicyControl{
					DecisionID: "bar",
				},
				Timestamp: "2020-01-10T21:00:11.663725026Z",
				Micros: pocolog.MicrosInfo{
					Version:    "1",
					Service:    "barservice",
					Deployment: "barz",
				},
			},
		},
		"bazservice": {
			{
				PolicyControl: pocolog.PolicyControl{
					DecisionID: "baz",
				},
				Timestamp: "2020-01-10T21:00:11.663725026Z",
				Micros: pocolog.MicrosInfo{
					Version:    "1",
					Service:    "bazservice",
					Deployment: "bazz",
				},
			},
		},
		"fooservice": {
			{
				PolicyControl: pocolog.PolicyControl{
					DecisionID: "foo",
				},
				Timestamp: "2020-01-10T21:00:11.663725026Z",
				Micros: pocolog.MicrosInfo{
					Version:    "1",
					Service:    "fooservice",
					Deployment: "fooz",
				},
			},
		},
	}
)

// Tests should be run in single threaded mode to allow for cleanup between tests
func TestMain(m *testing.M) {
	if value, ok := os.LookupEnv("REDIS_RAWURL"); ok {
		client = NewRedisClient(value, time.Hour)
	} else {
		client = NewRedisClient("redis://127.0.0.1:6379/0", time.Hour)
	}
	os.Exit(m.Run())
}

// runClean wraps around a test to ensure redis is in a clean state before running
func runClean(t *testing.T, name string, test func(*testing.T)) {
	setup()
	t.Run(name, test)
	teardown()
}

func setup() {
	_, _ = http.Get("http://localhost:8087/stop")
	_, err := client.client.Do("FLUSHALL")
	if err != nil {
		panic(err)
	}
}

func teardown() {
	_, _ = http.Get("http://localhost:8087/start")
}

func TestDefaultRedisClient(t *testing.T) {
	t.Run("AddNewLogs", testAddNewLogs)
	runClean(t, "GetLogsByIndex", testGetLogs)
	runClean(t, "ListServices", testListServices)
	runClean(t, "GetExpiredLogKeys", testGetExpiredLogKeys)
	runClean(t, "DeleteLogs", testDeleteLogs)
}

// TestDefaultRedisClient_AddNewLogs checks that the function does not return an err
// It does not test that the items are correctly inserted
func testAddNewLogs(t *testing.T) {
	_, err := client.AddNewLogs(inputs)
	if err != nil {
		t.Error(err)
	}
}

// TestDefaultRedisClient_GetLogs checks that we get all logs back from redis
// in the same form as we put it in.
func testGetLogs(t *testing.T) {
	client.AddNewLogs(inputs)
	type args struct {
		service string
	}
	tests := []struct {
		name    string
		args    args
		want    []pocolog.PocoLog
		wantErr bool
	}{
		{
			name: "fooservice",
			args: args{
				service: "fooservice",
			},
			want: []pocolog.PocoLog{
				{
					PolicyControl: pocolog.PolicyControl{
						DecisionID: "foo",
					},
					Timestamp: "2020-01-10T21:00:11.663725026Z",
					Micros: pocolog.MicrosInfo{
						Version:    "1",
						Service:    "fooservice",
						Deployment: "fooz",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "barservice",
			args: args{
				service: "barservice",
			},
			want: []pocolog.PocoLog{
				{
					PolicyControl: pocolog.PolicyControl{
						DecisionID: "bar",
					},
					Timestamp: "2020-01-10T21:00:11.663725026Z",
					Micros: pocolog.MicrosInfo{
						Version:    "1",
						Service:    "barservice",
						Deployment: "barz",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "bazservice",
			args: args{
				service: "bazservice",
			},
			want: []pocolog.PocoLog{
				{
					PolicyControl: pocolog.PolicyControl{
						DecisionID: "baz",
					},
					Timestamp: "2020-01-10T21:00:11.663725026Z",
					Micros: pocolog.MicrosInfo{
						Version:    "1",
						Service:    "bazservice",
						Deployment: "bazz",
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := client.GetLogs(tt.args.service)
			//t.Logf("%v", got)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetLogsByIndex() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetLogsByIndex() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// TestDefaultRedisClient_ListServices tests that we get back all the service names as expected
func testListServices(t *testing.T) {
	var (
		wantErr = false
		want    = []string{"barservice", "bazservice", "fooservice"}
	)
	client.AddNewLogs(inputs)
	got, err := client.ListServices()
	sort.Strings(got)
	if (err != nil) != wantErr {
		t.Errorf("ListServices() error = %v, wantErr %v", err, wantErr)
		return
	}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("ListServices() got = %v, want %v", got, want)
	}
}

func testGetExpiredLogKeys(t *testing.T) {
	testInputs := make(map[string][]pocolog.PocoLog)
	for k, v := range inputs {
		logsForService := make([]pocolog.PocoLog, 1)
		copy(logsForService, v)
		if k == "bazservice" {
			logsForService[0].Timestamp = time.Now().Format(time.RFC3339Nano)
		}
		testInputs[k] = logsForService
	}
	client.AddNewLogs(testInputs)
	got, err := client.GetExpiredLogKeys()
	if err != nil {
		t.Error(err)
	}
	expired := 0
	for _, v := range got {
		expired += len(v)
	}
	if expired != 2 {
		t.Errorf("Expected 2 expired logs, but got %d", len(got))
	}
}

func testDeleteLogs(t *testing.T) {
	client.AddNewLogs(inputs)
	expired, err := client.GetExpiredLogKeys()
	if err != nil {
		t.Error(err)
	}
	got, err := client.DeleteLogs(expired)
	if err != nil {
		t.Error(err)
	}
	if len(got) != len(inputs) {
		t.Errorf("DeleteLogs() returned %d logs, expected %d", len(got), len(inputs))
	}
	for service, v := range got {
		if !reflect.DeepEqual(v, inputs[service]) {
			t.Errorf("DeleteLogs() service: %s, error = %v, wantErr %v", service, v, inputs[service])
		}
	}
}

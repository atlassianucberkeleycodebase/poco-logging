package s3

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
	"golang.org/x/sync/errgroup"
)

type S3 interface {
	Region() string
	Name() string
	Client() s3iface.S3API

	DeleteObject(key string) error
	GetObject(key string) (*Object, error)
	SaveObject(*Object) error
	ObjectExists(key string) (bool, error)
	ListObjects(prefix string, limit, offset int) ([]*Object, error)
	DeepCheck() error
}

type DefaultS3Client struct {
	region   string
	name     string
	endpoint *string
	client   s3iface.S3API
}

type Object struct {
	Key      string
	Value    []byte
	Tags     map[string]string
	Metadata map[string]string
	Modified time.Time
}

func NewS3Client(region string, name string) *DefaultS3Client {
	c := &DefaultS3Client{
		region: region,
		name:   name,
	}
	c.client = c.Client()
	return c
}

func NewLocalClient(endpoint, region, name string) *DefaultS3Client {
	c := &DefaultS3Client{
		region:   region,
		name:     name,
		endpoint: aws.String(endpoint),
	}
	c.client = c.Client()
	return c
}

func (c *DefaultS3Client) Region() string {
	return c.region
}
func (c *DefaultS3Client) Name() string {
	return c.name
}

func (c *DefaultS3Client) Client() s3iface.S3API {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigDisable,
		Config: aws.Config{
			Region:           aws.String(c.Region()),
			Endpoint:         c.endpoint,
			S3ForcePathStyle: aws.Bool(true),
		},
	}))
	return s3.New(sess)
}

func (c *DefaultS3Client) DeleteObject(key string) error {
	_, err := c.client.DeleteObject(&s3.DeleteObjectInput{
		Bucket: aws.String(c.name),
		Key:    aws.String(key),
	})

	if err != nil {
		return err
	}
	return nil
}

func (c *DefaultS3Client) GetObject(key string) (*Object, error) {
	obj, err := c.client.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(c.name),
		Key:    aws.String(key),
	})

	if err != nil {
		return nil, err
	}

	blob, err := ioutil.ReadAll(obj.Body)
	if err != nil {
		return nil, err
	}

	var modified time.Time
	if obj.LastModified != nil {
		modified = *obj.LastModified
	}

	return &Object{
		Key:      key,
		Value:    blob,
		Modified: modified,
		Metadata: readAwsMap(obj.Metadata),
	}, nil
}

func (c *DefaultS3Client) SaveObject(obj *Object) error {
	stringTags := make([]string, 0, len(obj.Tags))
	for k, v := range obj.Tags {
		stringTags = append(stringTags, fmt.Sprintf("%s=%s", k, v))
	}

	s3Obj := &s3.PutObjectInput{
		ACL:          aws.String("public-read"),
		Bucket:       aws.String(c.name),
		Key:          aws.String(obj.Key),
		Body:         aws.ReadSeekCloser(bytes.NewReader(obj.Value)),
		ContentType:  aws.String("application/x-pem-file"),
		CacheControl: aws.String("max-age=600, stale-while-revalidate=1200"),
		Metadata:     writeAwsMap(obj.Metadata),
		Tagging:      aws.String(strings.Join(stringTags, "&")),
	}

	_, err := c.client.PutObject(s3Obj)
	return err
}

func (c *DefaultS3Client) ObjectExists(key string) (bool, error) {
	_, err := c.client.HeadObject(&s3.HeadObjectInput{
		Bucket: aws.String(c.name),
		Key:    aws.String(key),
	})

	if err == nil {
		return true, nil
	} else if aerr, ok := err.(awserr.Error); !ok && aerr.Code() != "NotFound" {
		return false, err
	}

	return false, nil
}

func (c *DefaultS3Client) ListObjects(prefix string, limit, offset int) ([]*Object, error) {
	objs := []*s3.Object{}
	err := c.client.ListObjectsV2Pages(&s3.ListObjectsV2Input{
		Bucket:  aws.String(c.name),
		Prefix:  aws.String(prefix),
		MaxKeys: aws.Int64(int64(limit + offset)),
	}, func(page *s3.ListObjectsV2Output, lastPage bool) bool {
		for _, obj := range page.Contents {
			if !strings.HasSuffix(*obj.Key, "/") {
				objs = append(objs, obj)
			}
		}
		return len(objs) < limit+offset
	})

	if err != nil {
		return nil, err
	}

	upper := offset + limit
	if upper > len(objs) {
		upper = len(objs)
	}
	keys := make([]*Object, upper-offset)

	group, _ := errgroup.WithContext(context.Background())

	for i := offset; i < upper; i++ {
		i := i
		group.Go(func() error {
			objectWithMetadata, err := c.objectWithMetadata(objs[i])
			if err != nil {
				return err
			}
			keys[i-offset] = objectWithMetadata
			return nil
		})
	}

	if err := group.Wait(); err != nil {
		return nil, err
	}

	return keys, nil
}

func (c *DefaultS3Client) DeepCheck() error {
	_, err := c.client.ListObjectsV2(&s3.ListObjectsV2Input{
		Bucket:  aws.String(c.name),
		MaxKeys: aws.Int64(1),
	})

	if err != nil {
		return err
	}
	return nil
}

func (c *DefaultS3Client) objectWithMetadata(obj *s3.Object) (*Object, error) {
	result, err := c.client.HeadObject(&s3.HeadObjectInput{
		Bucket: aws.String(c.name),
		Key:    obj.Key,
	})

	if err != nil {
		return nil, err
	}

	var modified time.Time
	if result.LastModified != nil {
		modified = *result.LastModified
	}

	return &Object{
		Key:      *obj.Key,
		Modified: modified,
		Metadata: readAwsMap(result.Metadata),
	}, nil
}

func readAwsMap(m map[string]*string) map[string]string {
	out := map[string]string{}
	for k, v := range m {
		if v != nil {
			// we always use lower case for our metadata ... something is upper casing things
			out[strings.ToLower(k)] = *v
		}
	}
	return out
}

func writeAwsMap(m map[string]string) map[string]*string {
	out := map[string]*string{}
	for k, v := range m {
		c := v
		out[k] = &c
	}
	return out

}

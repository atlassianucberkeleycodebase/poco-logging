package s3

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Region   string `envconfig:"S3_BUCKET_REGION" default:"us-northsouth-2"`
	Name     string `envconfig:"S3_BUCKET_NAME" default:"bucket"`
	Endpoint string `envconfig:"S3_BUCKET_ENDPOINT" default:"http://localhost:4572"`
}

func NewS3BucketClient() (S3, error) {
	config := Config{}
	if err := envconfig.Process("", &config); err != nil {
		return nil, err
	}
	fmt.Println("Name: ", config.Name)
	return NewLocalClient(config.Endpoint, config.Region, config.Name), nil
}

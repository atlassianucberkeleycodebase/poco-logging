package httpserver

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocorego"
	"github.com/open-policy-agent/opa/rego"
	"github.com/rs/zerolog/log"

	"github.com/go-chi/chi"
	"go.uber.org/zap"
)

type Config struct {
	// The listening address and port for the HTTP server to listen on
	Listen string

	// The duration to wait for a graceful shutdown
	ShutdownTimeout time.Duration
}

type HttpServerApplication struct {
	config      Config
	server      *http.Server
	router      chi.Router
	middlewares Middlewares
	service     Service
	log         *zap.Logger
}

func New(log *zap.Logger, config Config, middlewares Middlewares, service Service) (*HttpServerApplication, error) {
	m := &HttpServerApplication{
		config:      config,
		log:         log,
		middlewares: make(Middlewares, len(middlewares)),
		service:     service,
		router:      chi.NewRouter(),
		server:      nil,
	}

	copy(m.middlewares, middlewares)

	m.server = &http.Server{Addr: config.Listen, Handler: m.router}

	return m, nil
}

func (s *HttpServerApplication) SetupRouteAndStartServer() {

	s.router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("root"))
	})

	s.router.Post("/analyze", analyze)

	s.router.Post("/diff", diff)

	s.router.Get("/services", services)

	s.router.Get("/healthcheck", healthcheck)

	s.router.Get("/ping", ping)

	if err := http.ListenAndServe(s.config.Listen, s.router); err != nil {
		s.log.Fatal(err.Error())
	}
}

func healthcheck(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Write([]byte("healthcheck"))
}

func ping(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Write([]byte("pong"))
}

// analyze accepts a UserService, an input, and a query, combines them with platform,
// and returns the result of the OPA run
func analyze(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	// Parsing request json body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(400)
		return
	}
	req := &AnalyzeRequest{}
	err = json.Unmarshal(body, req)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return
	}

	// Getting/building the RegoPieces
	platform, err := pocorego.GetPlatform()
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}
	inputQuery := pocorego.RegoOptionsFromOptions(rego.Input(req.Input), rego.Query(req.Query))

	// Evaluate and write result to response
	res, err := pocorego.EvaluatePieces(&req.UserService, platform, inputQuery)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}
	resBytes, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write(resBytes)
	w.WriteHeader(200)
}

// diff accepts a UserService and a serviceName, fetches logs from that serviceName,
// runs the new policy against the inputs in the logs, and returns the difference between the old and new
// policy decisions
func diff(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	// TODO: Remove CORS hack
	w.Header().Set("Access-Control-Allow-Origin", "*")

	// Parsing request json body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Read Body: %v\n", err)
		w.WriteHeader(400)
		return
	}
	req := &DiffRequest{}
	err = json.Unmarshal(body, req)
	if err != nil {
		log.Printf("Unmarshal DiffRequest: %v\n", err)
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return
	}

	// Getting/building RegoPieces
	platform, err := pocorego.GetPlatform()
	if err != nil {
		log.Printf("GetPlatform: %v\n", err)
		w.WriteHeader(400)
		w.Write([]byte(err.Error()))
		return
	}

	queryString, err := pocorego.GetQuery()
	if err != nil {
		log.Printf("GetQuery: %v\n", err)
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}
	queryPiece := &pocorego.RegoOptions{Options: []func(*rego.Rego){rego.Query(queryString)}}

	// Parse modules, stores and query to prepare for evaluation of different inputs
	pq, err := pocorego.PrepareForEval(platform, &req.UserService, queryPiece)
	if err != nil {
		log.Printf("PrepareForEval: %v\n", err)
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}

	logs, err := redisClient.GetLogs(req.ServiceName)
	if err != nil {
		log.Printf("getLogs: %v\n", err)
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}

	var results DiffResults = &DiffResultsImpl{
		Results: make(map[string]*TopLayer),
	}

	// Runs inputs from logs against the prepared query and outputs diffs
	// Can potentially parallelize
	// goroutines run diffs and chan <- diff
	// main collects them all to concat and return results
	for _, testLog := range logs {
		// Parse input into a map
		input := rego.EvalInput(testLog.PolicyControl.Input)

		// Evaluate using prepared query
		res, err := pq.Eval(context.Background(), input)
		if err != nil {
			log.Printf("Eval: %v\n", err)
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
			return
		}

		// Check for diff
		resEqual, err := testLog.Difference(res)
		if err != nil {
			log.Printf("Difference: %v\n", err)
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
			return
		}

		err = results.AddLog(testLog, !resEqual)
		if err != nil {
			log.Printf("AddLog: %v\n", err)
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
			return
		}
	}
	res, err := results.Marshal()
	if err != nil {
		log.Printf("Marshal: %v\n", err)
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write(res)
}

// services returns the set of services available for running diffs on
func services(w http.ResponseWriter, r *http.Request) {
	services, err := redisClient.ListServices()
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("Internal server error: redisClient.ListServices"))
		log.Err(err)
	}
	res, err := json.Marshal(services)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("Internal server error: marshal"))
		log.Err(err)
	}
	w.Write(res)
}

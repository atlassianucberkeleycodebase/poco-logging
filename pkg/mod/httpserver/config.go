package httpserver

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

type serverConfig struct {
	// The listening address and port for the HTTP server to listen on
	Listen string `envconfig:"HTTP_LISTEN" default:":8081"`

	// The duration to wait for a graceful shutdown
	ShutdownTimeout time.Duration `envconfig:"HTTP_SHUTDOWN" default:"15s"`
}

func ServerConfig() Config {
	config := serverConfig{}
	if err := envconfig.Process("", &config); err != nil {
		panic(err)
	}
	return Config(config)
}

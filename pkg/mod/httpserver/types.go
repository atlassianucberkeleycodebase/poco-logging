package httpserver

import (
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocolog"
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocorego"
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"go.uber.org/zap"
	"net/http"
	"sort"
	"strconv"
)

type LoggerProvider interface {
	Logger() *zap.Logger
}

type ContextUpdater interface {
	UpdateContext(c context.Context) (context.Context, error)
}

type Middleware func(http.Handler) http.Handler

type Middlewares []func(http.Handler) http.Handler

type Service interface {
}

type Services []Service

func (s Services) InterfaceSlice() []interface{} {
	o := make([]interface{}, 0, len(s))
	for _, i := range s {
		o = append(o, i)
	}
	return o
}

type Identifiable interface {
	Service
	Identity() string
}

type Routable interface {
	Service
	Route(router chi.Router)
}

type Startable interface {
	Service
	Start(c context.Context) error
}

type Stoppable interface {
	Service
	Stop(c context.Context) error
}

type AnalyzeRequest struct {
	UserService pocorego.UserService   `json:"userService"`
	Input       map[string]interface{} `json:"input"`
	Query       string                 `json:"query"`
}

type DiffRequest struct {
	UserService pocorego.UserService `json:"userService"`
	ServiceName string               `json:"serviceName"`
}

type TopLayer struct {
	Content     TopContent    `json:"content"`
	HasChildren bool          `json:"hasChildren"`
	Children    []MiddleLayer `json:"children"`
}

type TopContent struct {
	Layer      string `json:"layer"`
	Endpoint   string `json:"endpoint"`
	HttpMethod string `json:"http"`
}

type MiddleLayer struct {
	Content     MiddleContent `json:"content"`
	HasChildren bool          `json:"hasChildren"`
	Children    []BottomLayer `json:"children"`
}

type MiddleContent struct {
	Layer      string `json:"layer"`
	DecisionID string `json:"id"`
	Mechanism  string `json:"mechanism"`
	Principals string `json:"principals"`
	Value      string `json:"value"`
	HasChanged bool   `json:"changed"`
}

type BottomLayer struct {
	Content BottomContent `json:"content"`
}

type BottomContent struct {
	Layer string `json:"layer"`
	Key   string `json:"header_key"`
	Value string `json:"header_value"`
}

// DiffResults stores the intermediate results from processing each log
type DiffResults interface {
	// AddLog should update the internal state of DiffResults
	AddLog(testLog pocolog.PocoLog, changed bool) error
	// Marshal should transform the internal state into the expected response format in a byte array
	Marshal() ([]byte, error)
}

// DiffResultsImpl implements the DiffResults interface and outputs a json in the atlaskit/TreeTable input schema
// A map is used so that we don't have to linear search every time we're inserting a new result
type DiffResultsImpl struct {
	// Map of {"<endpoint>:<method>" : TopLayer}
	Results map[string]*TopLayer
}

// AddLog adds the result of a new pocolog into the state of DiffResultsImpl
func (r *DiffResultsImpl) AddLog(testLog pocolog.PocoLog, changed bool) error {
	pocoInput, err := testLog.Input()
	if err != nil {
		return err
	}
	key := fmt.Sprintf("%s:%s", pocoInput.Path, pocoInput.Method)
	// Creating the top layer if it doesn't already exist
	if _, ok := r.Results[key]; !ok {
		r.Results[key] = &TopLayer{
			Content: TopContent{
				Layer:      "top",
				Endpoint:   pocoInput.Path,
				HttpMethod: pocoInput.Method,
			},
			HasChildren: true,
			Children:    make([]MiddleLayer, 0),
		}

		// Adding the middle layer table headers
		middleHeaders := MiddleLayer{
			Content: MiddleContent{
				Layer:      "header-middle",
				DecisionID: "ID",
				Mechanism:  "Mechanism",
				Principals: "Principals",
				Value:      "Value",
				HasChanged: false,
			},
			HasChildren: false,
			Children:    nil,
		}
		r.Results[key].Children = append(r.Results[key].Children, middleHeaders)
	}
	// Getting the new value of data.platform.authorize
	newValue := testLog.PolicyControl.Result[0].Expressions[0].Value.(string) == "true"
	if changed {
		newValue = !newValue
	}

	// Creating the middle layer
	middleLayer := MiddleLayer{
		Content: MiddleContent{
			Layer:      "middle",
			DecisionID: testLog.PolicyControl.DecisionID,
			Mechanism:  pocoInput.Mechanism,
			Principals: pocoInput.Principals,
			Value:      strconv.FormatBool(newValue),
			HasChanged: changed,
		},
		HasChildren: false,
		Children:    make([]BottomLayer, 0),
	}

	// If there are http headers, create the bottom layer(s)
	if len(testLog.PocoInput.Headers) > 0 {
		middleLayer.HasChildren = true

		// Adding bottom layer table headers
		bottomHeaders := BottomLayer{
			Content: BottomContent{
				Layer: "header-bottom",
				Key:   "Headers",
			},
		}
		middleLayer.Children = append(middleLayer.Children, bottomHeaders)

		// Adding each header to the bottom layer
		for key, value := range testLog.PocoInput.Headers {
			bottomLayer := BottomLayer{Content: BottomContent{
				Layer: "bottom",
				Key:   key,
				Value: value,
			}}
			middleLayer.Children = append(middleLayer.Children, bottomLayer)
		}
	}

	// Append the new result to the top layer
	r.Results[key].Children = append(r.Results[key].Children, middleLayer)

	return nil
}

// Marshal turns the DiffResults into the JSON format expected by the frontend
func (r *DiffResultsImpl) Marshal() ([]byte, error) {
	res := make([]TopLayer, 0)
	for _, v := range r.Results {
		res = append(res, *v)
	}
	// Sorting
	for _, topLayer := range res {
		if topLayer.HasChildren {
			for _, middleLayer := range topLayer.Children {
				if middleLayer.HasChildren {
					sort.Slice(middleLayer.Children, func(i, j int) bool {
						// We want the header-bottom row to be up top
						// And then sort by alphabetical order of http header keys
						return middleLayer.Children[i].Content.Layer == "header-bottom" ||
							(middleLayer.Children[i].Content.Key < middleLayer.Children[j].Content.Key && middleLayer.Children[j].Content.Layer != "header-bottom")
					})
				}
			}
			sort.Slice(topLayer.Children, func(i, j int) bool {
				// We want the header-middle row to be up top
				// Then, any hasChanged should go up top
				return topLayer.Children[i].Content.Layer == "header-middle" || (topLayer.Children[i].Content.HasChanged && topLayer.Children[j].Content.Layer != "header-middle")
			})
		}
	}
	sort.Slice(res, func(i, j int) bool {
		// Sort top layer alphabetically by endpoint
		return res[i].Content.Endpoint < res[j].Content.Endpoint
	})

	return json.Marshal(res)
}

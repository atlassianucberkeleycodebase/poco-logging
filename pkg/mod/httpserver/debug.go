package httpserver

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

type debugConfig struct {
	// The listening address and port for the HTTP server to listen on
	Listen string `envconfig:"DEBUG_LISTEN" default:":16686"`

	// The duration to wait for a graceful shutdown
	ShutdownTimeout time.Duration `envconfig:"DEBUG_SHUTDOWN" default:"15s"`
}

func DebugConfig() Config {
	config := debugConfig{}
	if err := envconfig.Process("", &config); err != nil {
		panic(err)
	}
	return Config(config)
}

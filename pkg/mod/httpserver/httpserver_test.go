package httpserver

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http/httptest"
	"testing"

	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocolog"
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocorego"
	"github.com/open-policy-agent/opa/rego"
)

func TestAnalyze(t *testing.T) {
	type analyzeTest struct {
		request AnalyzeRequest
		log     pocolog.PocoLog
	}
	// Setting up the tests
	tests := make([]analyzeTest, 0)
	for _, log := range pocorego.TestLogs {
		req := AnalyzeRequest{
			UserService: pocorego.UserService{
				UserPolicy: pocorego.ServiceServiceRego,
				UserData:   pocorego.ServiceDataJson,
			},
			Input: log.PolicyControl.Input,
			Query: log.PolicyControl.Query,
		}
		tests = append(tests, analyzeTest{
			request: req,
			log:     log,
		})
	}

	for _, test := range tests {
		req, err := json.Marshal(test.request)
		if err != nil {
			t.Error(err)
		}
		reqBody := bytes.NewBuffer(req)
		postReq := httptest.NewRequest("POST", "/analyze", reqBody)
		w := httptest.NewRecorder()
		analyze(w, postReq)

		resp := w.Result()
		body, _ := ioutil.ReadAll(resp.Body)

		res := &rego.ResultSet{}

		err = json.Unmarshal(body, res)
		if err != nil {
			t.Error(string(body))
		}
		equal, err := test.log.Difference(*res)

		if !equal {
			t.Errorf("unexpected result at log %s", test.log.PolicyControl.DecisionID)
		}
	}
}

func TestDiff(t *testing.T) {
	req := DiffRequest{
		UserService: pocorego.UserService{
			UserPolicy: pocorego.ServiceServiceRego,
			UserData:   pocorego.ServiceDataJson,
		},
		ServiceName: "service-a",
	}

	reqJson, err := json.Marshal(req)
	if err != nil {
		t.Error(err)
	}
	reqBody := bytes.NewBuffer(reqJson)
	postReq := httptest.NewRequest("POST", "/res", reqBody)
	w := httptest.NewRecorder()
	diff(w, postReq)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	diffs := make([]string, 0)

	res := make([]TopLayer, 0)
	err = json.Unmarshal(body, &res)
	for _, top := range res {
		for _, middle := range top.Children {
			if middle.Content.HasChanged {
				diffs = append(diffs, middle.Content.DecisionID)
			}
		}
	}
	if err != nil {
		t.Error(err)
	}
	if len(diffs) != 0 {
		t.Errorf("unexpected diffs: %v", diffs)
	}
}

package httpserver

import (
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/rediscache"
	"github.com/kelseyhightower/envconfig"
	"github.com/rs/zerolog/log"
)

var (
	redisClient rediscache.Redis
	redisConfig RedisConfig
)

type RedisConfig struct {
	RedisRawUrl string `envconfig:"REDIS_RAW_URL" default:"redis://localhost:6379/0"`
}

func init() {
	redisConfig = RedisConfig{}
	err := envconfig.Process("poco-logging", &redisConfig)
	if err != nil {
		log.Err(err)
	}
	redisClient = rediscache.NewRedisClient(redisConfig.RedisRawUrl, -1)
}

package queue

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
)

type SQSQueue interface {
	Region() string
	Name() string
	URL() string
	Client() sqsiface.SQSAPI

	ReceiveMessages(*sqs.ReceiveMessageInput) ([]*sqs.Message, error)
	DeleteMessage(receiptHandle *string) error
}

type DefaultSQSQueue struct {
	region   string
	name     string
	url      string
	endpoint *string
	client   sqsiface.SQSAPI
}

const (
	// MaxNumberOfMessage at one poll
	MaxNumberOfMessage int64 = 10

	// WaitTimeSecond for each poll
	WaitTimeSecond int64 = 20
)

func NewSQSQueue(region string, name string, url string) *DefaultSQSQueue {
	q := &DefaultSQSQueue{
		region: region,
		name:   name,
		url:    url,
	}
	q.client = q.Client()
	return q
}

func NewLocalQueue(endpoint, region, name, url string) *DefaultSQSQueue {
	q := &DefaultSQSQueue{
		region:   region,
		name:     name,
		url:      url,
		endpoint: aws.String(endpoint),
	}
	q.client = q.Client()
	return q
}

func (q *DefaultSQSQueue) Region() string {
	return q.region
}
func (q *DefaultSQSQueue) Name() string {
	return q.name
}
func (q *DefaultSQSQueue) URL() string {
	return q.url
}

func (q *DefaultSQSQueue) Client() sqsiface.SQSAPI {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigDisable,
		Config: aws.Config{
			Region:   aws.String(q.Region()),
			Endpoint: q.endpoint,
		},
	}))
	return sqs.New(sess)
}

func (q *DefaultSQSQueue) ReceiveMessages(in *sqs.ReceiveMessageInput) ([]*sqs.Message, error) {
	if in == nil {
		in = &sqs.ReceiveMessageInput{}
	}

	in.QueueUrl = aws.String(q.URL())

	if in.MaxNumberOfMessages == nil {
		in.MaxNumberOfMessages = aws.Int64(MaxNumberOfMessage)
	}

	if len(in.AttributeNames) == 0 {
		in.AttributeNames = []*string{
			aws.String("MessageDeduplicationId"),
			aws.String("MessageGroupId"),
		}
	}

	if len(in.MessageAttributeNames) == 0 {
		in.MessageAttributeNames = []*string{
			aws.String("All"),
		}
	}

	if in.WaitTimeSeconds == nil {
		in.WaitTimeSeconds = aws.Int64(WaitTimeSecond)
	}

	resp, err := q.client.ReceiveMessage(in)
	if err != nil {
		return nil, err
	}
	return resp.Messages, nil
}

func (q *DefaultSQSQueue) DeleteMessage(receiptHandle *string) error {
	_, err := q.client.DeleteMessage(&sqs.DeleteMessageInput{
		QueueUrl:      aws.String(q.URL()),
		ReceiptHandle: receiptHandle,
	})

	return err
}

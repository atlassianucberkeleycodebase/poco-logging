package pocorego

import (
	"encoding/json"
	"github.com/open-policy-agent/opa/rego"
	"github.com/open-policy-agent/opa/storage/inmem"
)

// RegoPiece defines an interface from which we can get rego Options.
// types that implement RegoPiece should transform their data into
// Rego Options (e.g. rego.Query, rego.Input, etc.) which can be provided to Rego.New(...)
type RegoPiece interface {
	GetOptions() ([]func(*rego.Rego), error)
}

// RegoOptions is a wrapper around an array of rego options that implements the RegoPiece interface
type RegoOptions struct {
	Options []func(*rego.Rego)
}

func (ro *RegoOptions) GetOptions() (options []func(*rego.Rego), err error) {
	options = ro.Options
	return
}

func RegoOptionsFromOptions(options ...func(*rego.Rego)) (piece *RegoOptions) {
	return &RegoOptions{Options: options}
}

// UserService represents a user defined OPA policy and implements RegoPiece
// The object will usually be constructed from user input on one of the front-ends
type UserService struct {
	UserPolicy string `json:"userPolicy"`
	UserData   string `json:"userData"`
}

// GetOptions processes the policy and data strings and returns an array of two rego Options
// The first function adds the service.rego module, which is the user defined rego policy
// The second function adds the json rules into the data.service namespace
// which is the expected behavior for the base policy.rego to function correctly
func (req *UserService) GetOptions() (service []func(*rego.Rego), err error) {
	// Initialize service as a slice of functions
	service = make([]func(*rego.Rego), 0)
	// Add in service.rego
	if len(req.UserPolicy) > 0 {
		service = append(service, rego.Module("service.rego", req.UserPolicy))
	} else {
		// If there is no provided .rego, default to no rego rules
		service = append(service, rego.Module("service.rego", ServiceEmptyServiceRego))
	}

	// Turn UserData (data.json) into a map
	data := make(map[string]interface{})
	err = json.Unmarshal([]byte(req.UserData), &data)
	if err != nil {
		return
	}

	// data.json needs to be rooted at data.service, so initialize a new map to get the correct store structure
	serviceData := make(map[string]interface{})
	serviceData["service"] = data

	// Create the store
	store := inmem.NewFromObject(serviceData)
	service = append(service, rego.Store(store))
	return
}

// AtlPlatform contains the methods to get common Atlassian-wide resources.
// It implements RegoPiece, which returns the Options representing the default access control policy package (platform)
type AtlPlatform struct {
	PlatformPackageUrl string `envconfig:"platform_package_url" default:""`
}

func (platform *AtlPlatform) GetOptions() (data []func(*rego.Rego), err error) {
	// TODO: Replace hardcoded policies with a way to get the latest policies
	data = []func(*rego.Rego){
		rego.Module("policy.rego", Policy),
		rego.Module("policy_test.rego", PolicyTest),
	}
	return
}

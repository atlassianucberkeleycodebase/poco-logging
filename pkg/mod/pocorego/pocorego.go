// Package pocorego provides the contract between frontend and backend as per UserService,
// methods for working with rego objects,
// and utility functions
//
// The shape of the Rego namespace should be as follows:
// root
//  |
//  +--- data
//  |    |
//  |    +--- platform (as defined in policy.rego:platform)
//  |    |
//  |    +--- service (user defined rules)
//  |         |
//  |         +--- service.rego objects
//  |         |
//  |         +--- data.json objects
//  |
//  +--- input
package pocorego

import (
	"bytes"
	"context"
	"github.com/kelseyhightower/envconfig"
	"github.com/open-policy-agent/opa/rego"
	"github.com/pkg/errors"
	"io"
	"os"
)

var (
	spec     *Specification
	platform *AtlPlatform
)

// Specification holds various settings for pocorego
type Specification struct {
	PlatformUrl string `envconfig:"platform_url" default:""`
	Query       string `envconfig:"query" default:"data.platform.authorize"`
}

func init() {
	spec = &Specification{}
	_ = envconfig.Process("poco", spec)
	platform = &AtlPlatform{
		PlatformPackageUrl: spec.PlatformUrl,
	}
}

// GetPlatform returns a platform object configured with environment variables
func GetPlatform() (platform *AtlPlatform, err error) {
	return platform, nil
}

func GetQuery() (query string, err error) {
	return spec.Query, nil
}

// PrepareForEval parses all inputs, modules, and query arguments in the RegoPieces in preparation
// of evaluating them
func PrepareForEval(pieces ...RegoPiece) (pq *rego.PreparedEvalQuery, err error) {
	options := make([]func(*rego.Rego), 0)
	for _, piece := range pieces {
		pieceOpts, err := piece.GetOptions()
		if err != nil {
			return nil, err
		}
		options = append(options, pieceOpts...)
	}
	preparedQuery, err := rego.New(options...).PrepareForEval(context.Background())
	if err != nil {
		return nil, err
	} else {
		return &preparedQuery, nil
	}
}

func EvaluatePiecesWithQuery(query string, pieces ...RegoPiece) (resultSet rego.ResultSet, err error) {
	opts := &RegoOptions{
		Options: []func(*rego.Rego){rego.Query(query)}[:],
	}
	pieces = append(pieces, opts)
	return EvaluatePieces(pieces...)
}

func EvaluatePieces(pieces ...RegoPiece) (resultSet rego.ResultSet, err error) {
	options := make([]func(*rego.Rego), 0)
	for _, piece := range pieces {
		pieceOpts, err := piece.GetOptions()
		if err != nil {
			return nil, err
		}
		options = append(options, pieceOpts...)
	}
	return EvaluateRegoOptions(options)
}

func EvaluateRegoOptions(options []func(*rego.Rego)) (resultSet rego.ResultSet, err error) {
	return rego.New(options...).Eval(context.Background())
}

// ReadFile reads the file at filename and returns the contents as a string
func ReadFile(filename string) (string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return "", errors.Wrapf(err, "ReadFile failed while reading %s", filename)
	}
	res := &bytes.Buffer{}
	_, err = io.Copy(res, f)
	if err != nil {
		return "", errors.Wrapf(err, "ReadFile failed while reading %s", filename)
	}
	return res.String(), err
}

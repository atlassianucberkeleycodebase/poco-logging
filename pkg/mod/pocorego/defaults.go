package pocorego

import (
	"encoding/json"

	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocolog"
	"github.com/open-policy-agent/opa/rego"
)

var (
	Policy = `package platform
import data.service
import input as req

# Keywords / Constants
others = "_others"
expected_principals = [ p | p := req.principals[_]; service[req.mechanism][req.method][p] ]
principals = expected_principals {
    expected_principals[0]
} else = ["_others"] {
    is_other_principal
} else = [] {
  true
}

authorize = "true" {
    principal_output == set()
    method_output == set()
    path_output == set()
    custom_policy == set()
} else = "false" {
    true
}

# Healthcheck check to validate bundles are setup
available = "true" {
  is_object(service)
} else = "false" {
  true
}

# Base evaluations
principal_output = invalid_principals
method_output = invalid_method
path_output = invalid_path


# User defined custom policy
custom_policy = service.deny {
  is_object(service)
  service.enabled
  is_set(service.deny)
# deprecated name for custom policy interface
} else = service.authorize {
  is_object(service)
  service.enabled
  is_set(service.authorize)
} else = set() {
  true
}

# Top level query to wrap three queries into one
deny[result] {
    result := {"authorize": authorize, "principal": principal_output, "method": method_output, "path": path_output }
}

# For the principal listed under a mechanism, does the request's method match one of the whitelisted methods?
invalid_method["Method is not authorized"] {
  not service[req.mechanism][req.method]
}

# Is the principal explicitly whitelisted or is '_others' specified under mechanism as a catch-all?
invalid_principals["Principal is not whitelisted for authorization"] {
  not principals[0]
}


# For the principal listed under a mechanism, does the request's path match one of the whitelisted paths?
invalid_path["Path is not authorized"] {
  paths := [ path | p := principals[_]
           path := service[req.mechanism][req.method][p].paths[_]
           glob.match(path, ["/"], req.path)]
  not paths[0]
}

mechanism_has_others(mechanism, method) {
    service[mechanism][method][others]
}

is_other_principal {
    not expected_principals[0]
    mechanism_has_others(req.mechanism, req.method)
}
`
	PolicyTest = `package platform

test = {
    "methods": {
        "Good_Subject": {"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "expected"}},
        "Bad_Subject": {"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "unexpected"}},
        "POST_method": {"principals": ["not-listed"], "method": "POST", "path": "/public/foo", "mechanism": "group"}
    }
}

test_combined_output {
  authorize == "true" with input as test.methods.Good_Subject
  authorize == "false" with input as test.methods.Bad_Subject
  authorize == "false" with input as test.methods.POST_method
}

test_available {
  available == "true"
}
`
	ServiceDataJson = `{
  "slauthtoken": {
    "GET": {
      "cbartowski": {
        "paths": ["/api/v1/**", "/api/v2/**", "/public/**"]
      },
      "swalker": {
        "paths": ["/api/v1/**", "/public/**"]
      },
      "_others": {
        "paths": ["/public/**"]
      }
    },
    "POST": {
      "cbartowski": {
        "paths": ["/api/v1/**", "/public/**"]
      },
      "_others": {
        "paths": ["/public/**"]
      }
    },
    "PUT": {
      "cbartowski": {
        "paths": ["/api/v1/**", "/public/**"]
      },
      "_others": {
        "paths": ["/public/**"]
      }
    }
  },
  "asap": {
    "GET": {
      "service-a": {
        "paths": ["/api/v1/**", "/api/v2/**", "/public/**"]
      },
      "service-b": {
        "paths": ["/api/v1/**", "/public/**"]
      },
      "_others": {
        "paths": ["/public/**"]
      }
    },
    "POST": {
      "service-a": {
        "paths": ["/api/v1/**", "/api/v2/**", "/public/**"]
      },
      "_others": {
        "paths": ["/public/**"]
      }
    },
    "PUT": {
      "service-a": {
        "paths": ["/api/v1/**", "/public/**"]
      },
      "_others": {
        "paths": ["/public/**"]
      }
    }
  },
  "build": {
    "GET": {
      "build-a": {
        "paths": ["/api/v1/**", "/api/v2/**", "/public/**"]
      },
      "build-b": {
        "paths": ["/api/v1/**", "/api/v2/**", "/public/**"]
      },
      "build-c": {
        "paths": ["/api/v1/**", "/api/v2/**", "/public/**"]
      },
      "_others": {
        "paths": ["/public/**"]
      }
    },
    "POST": {
      "build-a": {
        "paths": ["/api/v2/**", "/public/**"]
      },
      "build-b": {
        "paths": ["/api/v2/**", "/public/**"]
      },
      "build-c": {
        "paths": ["/api/v1/**", "/public/**"]
      },
      "_others": {
        "paths": ["/public/**"]
      }
    },
    "PUT": {
      "build-a": {
        "paths": ["/api/v2/**", "/public/**"]
      },
      "_others": {
        "paths": ["/public/**"]
      }
    }
  },
  "bypass": {
    "GET": {
      "_others": {
        "paths": ["/api/v1/**", "/api/v2/**", "/public/**"]
      }
    },
    "POST": {
      "_others": {
        "paths": ["/api/v1/**", "/api/v2/**", "/public/**"]
      }
    },
    "PUT": {
      "_others": {
        "paths": ["/api/v1/**", "/api/v2/**", "/public/**"]
      }
    }
  }
}
`
	ServiceServiceRego = `package service

import input as req

enabled = true

deny["issuer requires certail subject"] {
  req.principals[_] == "external-service"
  not req.headers["X-SLAuth-Subject"] == "expected"
}

deny["issuer only allowed until date X"] {
  req.mechanism == "slauthtoken"
  req.principal == "temp-user"
  time.now_ns() > 10000000000000
}
`

	ServiceEmptyServiceRego = `package service

import input as req

enabled = true
`
	TestLogs = []pocolog.PocoLog{
		{
			PolicyControl: pocolog.PolicyControl{
				DecisionID: `d4e65d9b-25d8-4224-a03b-75f2f23ba136`,
				Input:      convert(`{"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "expected"}}`),
				Query:      `data.platform.authorize`,
				Result: rego.ResultSet{
					rego.Result{
						Expressions: []*rego.ExpressionValue{
							{
								Value: "true",
								Text:  "data.platform.authorize",
								Location: &rego.Location{
									Row: 1,
									Col: 1,
								},
							},
						},
					},
				},
			},
		},
		{
			PolicyControl: pocolog.PolicyControl{
				DecisionID: `d28f4ef6-4c9a-440b-9fa9-db204304758b`,
				Input:      convert(`{"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "unexpected"}}`),
				Query:      `data.platform.authorize`,
				Result: rego.ResultSet{
					rego.Result{
						Expressions: []*rego.ExpressionValue{
							{
								Value: "false",
								Text:  "data.platform.authorize",
								Location: &rego.Location{
									Row: 1,
									Col: 1,
								},
							},
						},
					},
				},
			},
		},
		{
			PolicyControl: pocolog.PolicyControl{
				DecisionID: `b03231e1-c8c2-4409-b402-5be6527f4013`,
				Input:      convert(`{"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "expected"}}`),
				Query:      `data.platform.authorize`,
				Result: rego.ResultSet{
					rego.Result{
						Expressions: []*rego.ExpressionValue{
							{
								Value: "true",
								Text:  "data.platform.authorize",
								Location: &rego.Location{
									Row: 1,
									Col: 1,
								},
							},
						},
					},
				},
			},
		},
	}
)

func convert(input string) map[string]interface{} {
	jsonMap := make(map[string]interface{})
	err := json.Unmarshal([]byte(input), &jsonMap)
	if err != nil {
		panic(err)
	}
	return jsonMap
}

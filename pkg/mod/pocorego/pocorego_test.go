package pocorego

import (
	"testing"
)

// Test with examples given
func TestPlatformTest(t *testing.T) {
	platform, err := GetPlatform()
	if err != nil {
		t.Error(err)
	}

	service := &UserService{
		UserPolicy: ServiceServiceRego,
		UserData:   ServiceDataJson,
	}

	res, err := EvaluatePiecesWithQuery("data.platform.authorize with input as data.platform.test.methods[x]", platform, service)
	if err != nil {
		t.Error(err)
	}

	if len(res) != 3 {
		t.Errorf("number of tests incorrect, expected %d, actual %d", 3, len(res))
	}
	for _, result := range res {
		var expected string
		switch result.Bindings["x"] {
		case "Good_Subject":
			expected = "true"
		case "Bad_Subject":
			expected = "false"
		case "POST_method":
			expected = "false"
		}
		if result.Expressions[0].Value != expected {
			t.Errorf("test failed with binding %s", result.Bindings["x"])
		}
	}
}

// Testing with logs
func TestWithLogs(t *testing.T) {
	platform, err := GetPlatform()
	if err != nil {
		t.Error(err)
	}

	service := &UserService{
		UserPolicy: ServiceServiceRego,
		UserData:   ServiceDataJson,
	}

	for _, log := range TestLogs {
		res, err := EvaluatePiecesWithQuery("data.platform.authorize", platform, service, &log)
		if err != nil {
			t.Error(err)
		}
		resEq, err := log.Difference(res)
		if err != nil {
			t.Error(err)
		}
		if !resEq {
			t.Errorf("query on log decisionId %s returned different result", log.PolicyControl.DecisionID)
		}
	}
}

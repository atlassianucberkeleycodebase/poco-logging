package logger

import (
	"go.uber.org/zap"
)

func New(config zap.Config) (*zap.Logger, error) {

	logger, err := config.Build()
	if err != nil {
		return nil, err
	}

	zap.RedirectStdLog(logger)
	zap.ReplaceGlobals(logger)

	return logger, nil
}

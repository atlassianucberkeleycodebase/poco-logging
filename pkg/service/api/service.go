package api

import (
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/queue"
	"github.com/kelseyhightower/envconfig"
	"go.uber.org/zap"
)

type QueueConfig struct {
	Region   string `envconfig:"SQS_WORK_QUEUE_REGION"`
	Name     string `envconfig:"SQS_WORK_QUEUE_NAME"`
	URL      string `envconfig:"SQS_WORK_QUEUE_URL"`
	Endpoint string `envconfig:"SQS_WORK_QUEUE_ENDPOINT"`
}

type Service struct {
	log    *zap.Logger
	config *Config
	queue  queue.SQSQueue
}

func NewService(config *Config, log *zap.Logger) (*Service, error) {
	qconfig := QueueConfig{}
	if err := envconfig.Process("", &qconfig); err != nil {
		panic(err)
	}
	return &Service{
		log:    log,
		config: config,
		queue:  queue.NewLocalQueue(qconfig.Region, qconfig.Name, qconfig.URL, qconfig.Endpoint),
	}, nil
}

func (s *Service) Run() {
	// TODO
}

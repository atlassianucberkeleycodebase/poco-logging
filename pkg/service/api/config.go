package api

import (
	"github.com/kelseyhightower/envconfig"
)

type Config struct{}

func NewConfig() (*Config, error) {
	cfg := &Config{}
	if err := envconfig.Process("", cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}

#!/bin/sh
awslocal s3api create-bucket --bucket bucket
awslocal sqs create-queue --queue-name bucket-notifications
awslocal s3api put-bucket-notification-configuration --bucket bucket --notification-configuration '{
    "QueueConfigurations": [
        {
            "QueueArn": "arn:aws:sqs:us-east-1:000000000000:bucket-notifications",
            "Events": [
                "s3:ObjectCreated:*"
            ]
        }
    ]
}'

package main

import (
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/httpserver"
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/logger"
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/service/api"
	"github.com/pkg/errors"
)

var (
	Name = "codebase"

	Version = "0.0.1"
)

func main() {
	log, err := logger.New(logger.Config())
	panicIfError(err, "Failed to configure logger")

	cfg, err := api.NewConfig()
	panicIfError(err, "Failed to configure service")

	service, err := api.NewService(cfg, log)
	panicIfError(err, "Failed to configure service")

	httpserver, err := httpserver.New(log, httpserver.ServerConfig(), nil, service)
	panicIfError(err, "Failed to configure HTTP server")

	// this starts the server and listens on the port specified in the docker-compose file (port 8081)
	httpserver.SetupRouteAndStartServer()

	service.Run()
	log.Info("Exiting")

}

func panicIfError(err error, reason string) {
	if err != nil {
		panic(errors.Wrap(err, reason))
	}
}

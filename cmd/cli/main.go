package main

import (
	"fmt"
	"os"

	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/cli/analyze"
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/cli/diff"
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/cli/logs"
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/cli/services"
	"github.com/alecthomas/kingpin"
)

var (
	// Version is set during build (see Makefile).
	Version string
)

func main() {
	app := kingpin.New("poco", "Interact with the poco platform")
	if Version != "" {
		app = app.Version(Version)
	}

	logs.Mount(app)
	analyze.Mount(app)
	diff.Mount(app)
	services.Mount(app)

	_, err := app.Parse(os.Args[1:])
	if err != nil {
		fmt.Println(err.Error())
		fmt.Fprintf(os.Stderr, "command failed due to %s", err.Error())
		os.Exit(2)
	}
}

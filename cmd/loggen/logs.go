package main

import (
	"encoding/json"
	"math/rand"
	"time"

	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocolog"
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocorego"

	"github.com/google/uuid"
	"github.com/open-policy-agent/opa/plugins/logs"
)

const (
	maxDuration = 1000000
)

type EC2Info struct {
	Stack string `json:"sn"`
	ID    string `json:"id"`
}

type MicrosInfo struct {
	Version    string `json:"sv"`
	Service    string `json:"si"`
	Deployment string `json:"di"`
}

type LogStructure struct {
	Timestamp string       `json:"timestamp"`
	Env       string       `json:"env"`
	EC2       EC2Info      `json:"ec2"`
	Micros    MicrosInfo   `json:"m"`
	Logs      logs.EventV1 `json:"policyControl"`
}

var (
	services    = []string{"service-a", "service-b", "service-c"}
	methods     = []string{"GET", "POST", "PUT"}
	deployments = []string{"depId"}
	versions    = []string{"v1", "v2", "v3"}
	stacks      = []string{"stack-1", "stack-2", "stack-3"}
	ids         = []string{"i-1234", "i-5678", "i-9012"}
	mechanisms  = []string{"asap", "build", "slauthtoken", "bypass"}
	users       = []string{"cbartowski", "swalker", "mgrimes"}
	builds      = []string{"build-a", "build-b", "build-c"}
	paths       = []string{"/api/v1/object", "/public/healthcheck", "/public/ping", "/api/v1/other", "/api/v2/diff"}
	headers     = map[string][]string{
		"X-SLAuth-Subject": []string{"expected", "unexpected"},
		"Authorization": []string{
			"Bearer f0af17449a83681de22db7ce16672f16f37131bec0022371d4ace5d1854301e0",
			"Bearer 3905abbe3417119a08bee6945710c576838ea4b6d68d0d9e436baa1e8886ac5f",
			"Bearer ad135933f34b95e675259b67775baf73e7da097a65e19bcc32606e050f560116",
		},
		"Content-Type": []string{
			"application/json",
			"application/x-www-form-urlencoded",
			"text/html",
			"multipart/form-data",
		},
		"Referer": []string{
			"https://atlaskit.atlassian.com/",
			"https://www.atlassian.com/software/jira",
			"https://bitbucket.org/dashboard/overview",
			"https://www.atlassian.com/software/confluence",
		},
	}
	headerKeys = []string{"X-SLAuth-Subject", "Authorization", "Content-Type", "Referer"}
)

func generateLogs(volume int) []LogStructure {
	res := make([]LogStructure, 0, volume)
	for i := 0; i < volume; i++ {
		platform, err := pocorego.GetPlatform()
		if err != nil {
			return nil
		}

		service := &pocorego.UserService{
			UserPolicy: pocorego.ServiceServiceRego,
			UserData:   pocorego.ServiceDataJson,
		}

		randomInput := randomInput()

		query := "data.platform.authorize"
		decisionID := uuid.New().String()
		testLog := pocolog.PocoLog{
			PolicyControl: pocolog.PolicyControl{
				DecisionID: decisionID,
				Input:      (*randomInput).(map[string]interface{}),
				Query:      query,
			},
		}

		var presult interface{}
		var result []map[string]interface{}
		eval, err := pocorego.EvaluatePiecesWithQuery(query, platform, service, &testLog)
		if err != nil {
			return nil
		}
		out, err := json.Marshal(eval)
		if err != nil {
			return nil
		}
		err = json.Unmarshal(out, &result)
		if err != nil {
			return nil
		}
		presult = result

		res = append(res, LogStructure{
			Timestamp: time.Now().UTC().Format(time.RFC3339Nano),
			Env:       "prod-east",
			Micros: MicrosInfo{
				Service:    random(services),
				Deployment: random(deployments),
				Version:    random(versions),
			},
			EC2: EC2Info{
				Stack: random(stacks),
				ID:    random(ids),
			},
			Logs: logs.EventV1{
				DecisionID: decisionID,
				Revision:   random(versions),
				Path:       random(paths),
				Query:      query,
				Input:      randomInput,
				Result:     &presult,
				Timestamp:  time.Now(),
				Metrics:    metrics(),
			},
		})
	}

	return res
}

func random(s []string) string {
	return s[rand.Intn(len(s))]
}

func randomInput() *interface{} {
	mechanism := random(mechanisms)
	res := map[string]interface{}{
		"mechanism": mechanism,
		"method":    random(methods),
		"path":      random(paths),
	}

	switch mechanism {
	case "slauthtoken":
		res["principals"] = []string{random(users)}
	case "asap":
		res["principals"] = []string{random(services)}
	case "build":
		res["principals"] = []string{random(builds)}
	case "bypass":
		res["principals"] = []string{"bypass"}
	}

	res["headers"] = randomHeaders()

	var out interface{}
	out = res

	return &out
}

func randomHeaders() (res map[string]string) {
	res = make(map[string]string)
	numHeaders := rand.Intn(4)
	rand.Shuffle(len(headerKeys), func(i, j int) { headerKeys[i], headerKeys[j] = headerKeys[j], headerKeys[i] })
	for i := 0; i < numHeaders; i++ {
		res[headerKeys[i]] = random(headers[headerKeys[i]])
	}
	return
}

func metrics() map[string]interface{} {
	return map[string]interface{}{
		"timer_rego_input_parse_ns":    rand.Intn(maxDuration),
		"timer_rego_module_compile_ns": rand.Intn(maxDuration),
		"timer_rego_module_parse_ns":   rand.Intn(maxDuration),
		"timer_rego_query_compile_ns":  rand.Intn(maxDuration),
		"timer_rego_query_eval_ns":     rand.Intn(maxDuration),
		"timer_rego_query_parse_ns":    rand.Intn(maxDuration),
	}
}

func convert(input string) map[string]interface{} {
	jsonMap := make(map[string]interface{})
	err := json.Unmarshal([]byte(input), &jsonMap)
	if err != nil {
		panic(err)
	}
	return jsonMap
}

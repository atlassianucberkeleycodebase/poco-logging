package main

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
	"github.com/google/uuid"
)

var (
	s3Url  = "http://aws:4572"
	s3Name = "bucket"
	region = "us-northsouth-2"
	client s3iface.S3API
)

func init() {
	if envvar, ok := os.LookupEnv("S3_BUCKET_NAME"); ok {
		s3Name = envvar
	}

	if envvar, ok := os.LookupEnv("S3_BUCKET_REGION"); ok {
		region = envvar
	}

	if envvar, ok := os.LookupEnv("S3_BUCKET_ENDPOINT"); ok {
		s3Url = envvar
	}

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigDisable,
		Config: aws.Config{
			Region:           aws.String(region),
			Endpoint:         aws.String(s3Url),
			S3ForcePathStyle: aws.Bool(true),
		},
	}))
	client = s3.New(sess)
}

func writeLogs(logs []LogStructure) error {
	data := make([]string, 0, len(logs))
	for _, l := range logs {
		b, err := json.Marshal(l)
		if err != nil {
			return err
		}
		data = append(data, string(b))
	}

	var buf bytes.Buffer
	zw := gzip.NewWriter(&buf)

	_, err := zw.Write([]byte(strings.Join(data, "\n")))
	if err != nil {
		return err
	}

	if err := zw.Close(); err != nil {
		return err
	}

	s3Obj := &s3.PutObjectInput{
		Bucket: aws.String(s3Name),
		Key:    aws.String(uuid.New().String()),
		Body:   aws.ReadSeekCloser(bytes.NewReader(buf.Bytes())),
	}

	_, err = client.PutObject(s3Obj)
	return err
}

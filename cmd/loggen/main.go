package main

/**

  Very simple log generator to mock out the behaviour exhibited by a normal poco container
  Not a good example of a proper service

**/

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

var (
	emitLogs    = false
	logInterval = 5
	logVolume   = 10
)

func init() {
	if envvar, ok := os.LookupEnv("RUN_LOGS"); ok && envvar == "true" {
		emitLogs = true
	}

	var err error
	if envvar, ok := os.LookupEnv("LOG_INTERVAL"); ok {
		logInterval, err = strconv.Atoi(envvar)
		if err != nil {
			panic(fmt.Sprintf("LOG_INTERVAL is not valid %s", err.Error()))
		}
	}

	if envvar, ok := os.LookupEnv("LOG_VOLUME"); ok {
		logVolume, err = strconv.Atoi(envvar)
		if err != nil {
			panic(fmt.Sprintf("LOG_VOLUME is not valid %s", err.Error()))
		}
	}
}

func main() {
	go runLoggen()

	http.HandleFunc("/", RequestHandler)
	http.ListenAndServe(":8080", nil)
}

func RequestHandler(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/start":
		emitLogs = true
		log.Println("Received start request")
		w.WriteHeader(http.StatusAccepted)
	case "/stop":
		emitLogs = false
		log.Println("Received stop request")
		w.WriteHeader(http.StatusAccepted)
	default:
		log.Printf("Received unknown request %s", r.URL.Path)
		w.WriteHeader(http.StatusNotFound)
	}
}

func runLoggen() {
	for {
		time.Sleep(5 * time.Second)
		if !emitLogs {
			continue
		}

		logs := generateLogs(logVolume)
		err := writeLogs(logs)
		if err != nil {
			log.Printf("Failed to write logs %s", err.Error())
		} else {
			log.Print("Successfully wrote logs")
		}
	}
}

package main

import (
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocolog"
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/rediscache"
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/s3"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"log"
	"time"
)

var (
	S3client    s3.S3
	RedisClient rediscache.Redis
)

type Config struct {
	RedisRawUrl    string        `envconfig:"REDIS_RAW_URL" default:"redis://127.0.0.1:6379/0"`
	RedisLogExpiry time.Duration `envconfig:"REDIS_LOG_EXPIRY" default:"1h"`
}

func init() {
	config := &Config{}
	err := envconfig.Process("poco-logging", config)
	if err != nil {
		panic(err)
	}
	RedisClient = rediscache.NewRedisClient(config.RedisRawUrl, config.RedisLogExpiry)
}

// formatUpdates is a helper function to turn updates into a debug string
func formatUpdates(updates map[string]int, isAdd bool) (res string) {
	switch isAdd {
	case true:
		res = "Added logs to cache: "
	case false:
		res = "Removed logs from cache: "
	}
	for k, v := range updates {
		res += fmt.Sprintf("%s: %d ", k, v)
	}
	return res
}

// preprocessOnce does one iteration of the preprocessing job:
// It gets messages from SQS, retrieves all of the logs corresponding to those messages from S3,
// splits them by service, and adds them to redis
func preprocessOnce() error {
	// Get newly inserted logs messages
	messages, err := getMessages()
	if err != nil {
		return err
	}
	testLogs := make([]pocolog.PocoLog, 0)
	// Getting all of the testLogs for the messages from S3
	for _, message := range messages {
		for _, record := range message.Records {
			newLogs, err := pocolog.GetLogsByKey(record.S3.Object.Key)
			if err != nil {
				return err
			}
			testLogs = append(testLogs, newLogs...)
		}
	}
	// Split and insert into Redis
	splitByService := split(testLogs)
	updates, err := RedisClient.AddNewLogs(splitByService)
	if err != nil {
		return err
	}
	log.Print(formatUpdates(updates, true))
	// Delete the messages from SQS queue
	err = delMessages(messages)
	if err != nil {
		return err
	}
	return nil
}

// runPreprocess loops preprocessOnce forever
func runPreprocess(errChan chan error) {
	for {
		time.Sleep(time.Second * 10)
		err := preprocessOnce()
		if err != nil {
			errChan <- err
		}
	}
}

// trimOnce trims expired logs from the cache and writes them back to S3
func trimOnce() error {
	expired, err := RedisClient.GetExpiredLogKeys()
	if err != nil {
		return err
	}
	deleted, err := RedisClient.DeleteLogs(expired)
	if err != nil {
		return err
	}
	err = writeLogsToS3(deleted)
	if err != nil {
		return err
	}
	updates := make(map[string]int)
	for service, testLogs := range deleted {
		updates[service] = len(testLogs)
	}
	log.Print(formatUpdates(updates, false))
	return nil
}

func runTrim(errChan chan error) {
	for {
		time.Sleep(time.Minute)
		err := trimOnce()
		if err != nil {
			errChan <- err
		}
	}
}

func main() {
	errors := make(chan error)
	go runPreprocess(errors)
	go runTrim(errors)
	for err := range errors {
		log.Print(err)
	}
	return
}

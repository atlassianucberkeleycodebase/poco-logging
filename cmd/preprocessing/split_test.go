package main

import (
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocolog"
	"reflect"
	"testing"
)

var (
	ParsedLogs = []pocolog.PocoLog{
		{
			PolicyControl: pocolog.PolicyControl{
				DecisionID: `d4e65d9b-25d8-4224-a03b-75f2f23ba136`,
			},
			Timestamp: "2020-01-10T21:00:11.663725026Z",
			Micros: pocolog.MicrosInfo{
				Version:    "1",
				Service:    "fooservice",
				Deployment: "fooz",
			},
		},
		{
			PolicyControl: pocolog.PolicyControl{
				DecisionID: `d28f4ef6-4c9a-440b-9fa9-db204304758b`,
			},
			Timestamp: "2020-01-10T21:00:11.663725026Z",
			Micros: pocolog.MicrosInfo{
				Version:    "1",
				Service:    "barservice",
				Deployment: "barz",
			},
		},
		{
			PolicyControl: pocolog.PolicyControl{
				DecisionID: `b03231e1-c8c2-4409-b402-5be6527f4013`,
			},
			Timestamp: "2020-01-10T21:00:11.663725026Z",
			Micros: pocolog.MicrosInfo{
				Version:    "1",
				Service:    "bazservice",
				Deployment: "bazz",
			},
		},
	}
	splitByService = map[string][]pocolog.PocoLog{
		"fooservice": []pocolog.PocoLog{
			{
				PolicyControl: pocolog.PolicyControl{
					DecisionID: `d4e65d9b-25d8-4224-a03b-75f2f23ba136`,
				},
				Timestamp: "2020-01-10T21:00:11.663725026Z",
				Micros: pocolog.MicrosInfo{
					Version:    "1",
					Service:    "fooservice",
					Deployment: "fooz",
				},
			},
		},
		"barservice": []pocolog.PocoLog{
			{
				PolicyControl: pocolog.PolicyControl{
					DecisionID: `d28f4ef6-4c9a-440b-9fa9-db204304758b`,
				},
				Timestamp: "2020-01-10T21:00:11.663725026Z",
				Micros: pocolog.MicrosInfo{
					Version:    "1",
					Service:    "barservice",
					Deployment: "barz",
				},
			},
		},
		"bazservice": []pocolog.PocoLog{
			{
				PolicyControl: pocolog.PolicyControl{
					DecisionID: `b03231e1-c8c2-4409-b402-5be6527f4013`,
				},
				Timestamp: "2020-01-10T21:00:11.663725026Z",
				Micros: pocolog.MicrosInfo{
					Version:    "1",
					Service:    "bazservice",
					Deployment: "bazz",
				},
			},
		},
	}
)

func TestSplit(t *testing.T) {
	res := split(ParsedLogs)
	if !reflect.DeepEqual(res, splitByService) {
		t.Errorf("Split() got = %v, want = %v", res, splitByService)
	}
}

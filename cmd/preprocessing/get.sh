go get github.com/kelseyhightower/envconfig
go get github.com/gomodule/redigo/redis
go get github.com/aws/aws-sdk-go/aws
go get github.com/google/uuid
go get github.com/jmespath/go-jmespath
go get github.com/open-policy-agent/opa/rego
go get golang.org/x/sync/errgroup
go get github.com/rs/zerolog/log
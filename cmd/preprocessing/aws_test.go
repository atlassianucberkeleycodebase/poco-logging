package main

import (
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocolog"
	"testing"
)

var (
	inputs = map[string][]pocolog.PocoLog{
		"fooservice": {
			{
				PolicyControl: pocolog.PolicyControl{
					DecisionID: "foo",
				},
				Timestamp: "2020-01-10T21:00:11.663725026Z",
				Micros: pocolog.MicrosInfo{
					Version:    "1",
					Service:    "fooservice",
					Deployment: "fooz",
				},
			},
		},
		"barservice": {
			{
				PolicyControl: pocolog.PolicyControl{
					DecisionID: "bar",
				},
				Timestamp: "2020-01-10T21:00:11.663725026Z",
				Micros: pocolog.MicrosInfo{
					Version:    "1",
					Service:    "barservice",
					Deployment: "barz",
				},
			},
		},
		"bazservice": {
			{
				PolicyControl: pocolog.PolicyControl{
					DecisionID: "baz",
				},
				Timestamp: "2020-01-10T21:00:11.663725026Z",
				Micros: pocolog.MicrosInfo{
					Version:    "1",
					Service:    "bazservice",
					Deployment: "bazz",
				},
			},
		},
	}
)

func TestGetMessages(t *testing.T) {
	res, err := getMessages()
	if err != nil {
		t.Error(err)
	}
	t.Logf("%v", res)
}

func TestWriteLogs(t *testing.T) {
	err := writeLogsToS3(inputs)
	if err != nil {
		t.Error(err)
	}
}

package main

import (
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocolog"
)

// split accepts an array of TestLogs and splits into different services
func split(array []pocolog.PocoLog) (splitByService map[string][]pocolog.PocoLog) {
	m := make(map[string][]pocolog.PocoLog)
	for _, log := range array {
		if _, initialized := m[log.Micros.Service]; !initialized {
			var init []pocolog.PocoLog
			m[log.Micros.Service] = init
		}
		m[log.Micros.Service] = append(m[log.Micros.Service], log)
	}
	return m
}

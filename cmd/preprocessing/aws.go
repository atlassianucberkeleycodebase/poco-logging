package main

import (
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/pocolog"
	"bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/queue"
	pocoS3 "bitbucket.org/atlassianucberkeleycodebase/poco-logging/pkg/mod/s3"
	"bytes"
	"compress/gzip"
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/google/uuid"
	"github.com/kelseyhightower/envconfig"
	"strings"
)

type AWSConfig struct {
	S3BucketName         string `envconfig:"S3_BUCKET_NAME" default:"bucket"`
	S3BucketRegion       string `envconfig:"S3_BUCKET_REGION" default:"us-northsouth-2"`
	S3BucketEndpoint     string `envconfig:"S3_BUCKET_ENDPOINT" default:"http://localhost:4572"`
	SQSWorkQueueName     string `envconfig:"SQS_WORK_QUEUE_NAME" default:"bucket-notifications"`
	SQSWorkQueueRegion   string `envconfig:"SQS_WORK_QUEUE_REGION" default:"us-east-1"`
	SQSWorkQueueEndpoint string `envconfig:"SQS_WORK_QUEUE_ENDPOINT" default:"http://localhost:4576"`
	SQSWorkQueueUrl      string `envconfig:"SQS_WORK_QUEUE_URL" default:"http://localhost:4576/queue/bucket-notifications"`
}

var (
	config   *AWSConfig
	sqs      queue.SQSQueue
	s3Client pocoS3.S3
)

func init() {
	config = &AWSConfig{}
	err := envconfig.Process("poco-logging", config)
	if err != nil {
		panic(err)
	}
	sqs = queue.NewLocalQueue(config.SQSWorkQueueEndpoint, config.SQSWorkQueueRegion,
		config.SQSWorkQueueName, config.SQSWorkQueueUrl)
	s3Client = pocoS3.NewLocalClient(config.S3BucketEndpoint, config.S3BucketRegion, config.S3BucketName)
}

// sqsMessage represents an update message from the sqs queue
type sqsMessage struct {
	Records       []sqsMessageRecord
	ReceiptHandle string `json:"-"`
}

type sqsMessageRecord struct {
	S3 struct {
		Bucket struct {
			Name          string `json:"name"`
			Arn           string `json:"arn"`
			OwnerIdentity struct {
				PrincipalId string `json:"principalId"`
			} `json:"ownerIdentity"`
		} `json:"bucket"`
		Object struct {
			Key       string `json:"key"`
			Size      int    `json:"size"`
			ETag      string `json:"eTag"`
			VersionId string `json:"versionId"`
			Sequencer string `json:"sequencer"`
		} `json:"object"`
	} `json:"s3"`
}

func getMessages() ([]sqsMessage, error) {
	messages, err := sqs.ReceiveMessages(nil)
	if err != nil {
		return nil, err
	}
	out := make([]sqsMessage, 0)
	for _, msg := range messages {
		message := &sqsMessage{}
		err := json.Unmarshal([]byte(*msg.Body), message)
		if err != nil {
			return nil, err
		}
		message.ReceiptHandle = *msg.ReceiptHandle
		out = append(out, *message)
	}
	return out, nil
}

func delMessages(messages []sqsMessage) error {
	for _, message := range messages {
		err := sqs.DeleteMessage(&message.ReceiptHandle)
		if err != nil {
			return err
		}
	}
	return nil
}

func writeLogsToS3(logsByService map[string][]pocolog.PocoLog) error {
	for service, testLogs := range logsByService {
		// Preemptively create the bucket for the service as it might not exist
		_, err := s3Client.Client().CreateBucket(&s3.CreateBucketInput{Bucket: &service})
		if err != nil {
			// Ignore errors related to bucket already existing
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case s3.ErrCodeBucketAlreadyExists:
				case s3.ErrCodeBucketAlreadyOwnedByYou:
				default:
					return aerr
				}
			} else {
				return err
			}
		}
		data := make([]string, 0, len(testLogs))
		for _, log := range testLogs {
			b, err := json.Marshal(log)
			if err != nil {
				return err
			}
			data = append(data, string(b))
		}

		var buf bytes.Buffer
		zw := gzip.NewWriter(&buf)

		_, err = zw.Write([]byte(strings.Join(data, "\n")))
		if err != nil {
			return err
		}

		if err := zw.Close(); err != nil {
			return err
		}

		s3Obj := &s3.PutObjectInput{
			Bucket: aws.String(service),
			Key:    aws.String(uuid.New().String()),
			Body:   aws.ReadSeekCloser(bytes.NewReader(buf.Bytes())),
		}

		_, err = s3Client.Client().PutObject(s3Obj)
		if err != nil {
			return err
		}
	}
	return nil
}

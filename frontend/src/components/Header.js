import React from "react";
import "../css/App.css";
import EmojiAtlassianIcon from '@atlaskit/icon/glyph/emoji/atlassian';

export default class Sidebar extends React.Component {
  render() {
    return (
      <header className="App-header">
        <br/>
        <EmojiAtlassianIcon size="xlarge"></EmojiAtlassianIcon>
      </header>
    );
  }
}

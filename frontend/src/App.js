import React from "react";

import Editor from "./Editor";

import "./css/App.css";

export default class App extends React.Component {
  resize = () => this.forceUpdate();

  componentDidMount() {
    window.addEventListener("resize", this.resize);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.resize);
  }

  render() {
    return <Editor />;
  }
}
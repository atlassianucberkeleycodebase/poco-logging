import React from "react";
import Textfield from "@atlaskit/textfield";
import TextArea from "@atlaskit/textarea";
import Pagination from "@atlaskit/pagination";
import Select from "@atlaskit/select";
import Button from "@atlaskit/button";
import Tag from "@atlaskit/tag";
import Popup from "@atlaskit/popup";
import TagGroup from "@atlaskit/tag-group";
import QuestionCircleIcon from "@atlaskit/icon/glyph/question-circle";

import {
  generateEndpointRow,
  generateLogRow,
  generateHeadersRow,
  generateTableHeaders,
  filterAllLogs,
  userInstructions,
} from "./TableTreeUtils.js";
import TableTree, { Headers, Header, Rows } from "@atlaskit/table-tree";
import axios from "axios";
import Sidebar from "./components/Header";
import "./css/Editor.css";

// The main class for the Editor page.
export default class Editor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: "",
      rego: "",
      json: "",
      info: [],
      paginatedInfo: null,
      pageNumber: 1,
      pages: [],
      filters: [],
      popupDisplayed: true,
      services: [],
      currentService: "",
    };
  }

  // Paginate LOGS, with ITEMSPERPAGE items per page.
  paginateLogs(logs) {
    const itemsPerPage = 4;
    var numPages = Math.ceil(logs.length / itemsPerPage);
    var pages = [...Array(numPages + 1).keys()].slice(1);

    var result = [];
    for (var i = 0; i <= numPages; i++) {
      var start = i * itemsPerPage;
      var currentSlice = logs.slice(start, start + itemsPerPage);
      result[i] = currentSlice;
    }
    this.setState({ paginatedInfo: result, pages: pages });
  }

  // Process file from html input and save to state at [dest].
  handleFileUpload(event, dest) {
    this.setState({ file: event.target.files[0] }, () => {
      var reader = new FileReader();
      // After file is loaded into reader, set state at [dest].
      reader.onload = (e) => {
        const content = reader.result;
        this.setState({ [dest]: content });
        this.getDiff();
      };
      if (this.state.file) {
        var extension = this.state.file.name.split(".").pop();
        if (extension !== dest) {
          alert(
            "File upload was expecting file type of: ." +
              dest +
              " but received file type of: ." +
              extension
          );
        } else {
          reader.readAsText(this.state.file);
        }
      }
    });
  }

  // call diff and runs past logs against the policy
  getDiff() {
    // Request from files
    const request = {
      userService: {
        userData: this.state.json,
        userPolicy: this.state.rego,
      },
      serviceName: this.state.currentService,
    };

    axios
      .post("/diff", request)
      .then((result) => {
        this.setState({ info: result.data }, () => {
          this.paginateLogs(filterAllLogs(result.data, this.state.filters));
          this.setState({ unfilteredLogs: result.data });
        });
      })
      .catch((error) => {
        console.log("Axios request failed: " + error);
      });
  }

  changeNum = (event, newPage) => {
    this.setState({ pageNumber: newPage });
  };

  componentDidMount() {
    axios.get("/services").then((result) => {
      this.setState({ services: result.data.sort() });
    });
  }

  updateFilter(newValue) {
    this.setState({ filterBy: newValue.value });
  }

  updateService(newValue, _) {
    this.setState({ currentService: newValue.value }, () => {
      if (this.state.json !== "" && this.state.rego !== "") {
        this.getDiff();
      }
    });
  }

  addFilter(attribute, value) {
    // test if there is a filter with the same attribute and value already
    var matching = false;
    for (var a = 0; a < this.state.filters.length; a += 1) {
      if (this.state.filters[a][0].toLowerCase() === attribute.toLowerCase()) {
        // detect if there is a matching filter
        matching = true;
      }
    }
    // don't add if there is a matching filter
    if (!matching) {
      this.setState({ pageNumber: 1 });
      this.state.filters.push([attribute, value]);
    }
    // refilter logs
    return this.paginateLogs(
      filterAllLogs(this.state.unfilteredLogs, this.state.filters)
    );
  }

  // Return a Tag component displaying CATEGORY and VALUE.
  generateTag(category, value) {
    return (
      <Tag
        text={category + ": " + value}
        removeButtonText="Remove"
        key={category}
        onBeforeRemoveAction={() => {
          this.handleRemove(category, value);
        }}
      />
    );
  }

  // handles removing a tag with category CATEGORY and value VALUE
  handleRemove(category, value) {
    for (var i = 0; i < this.state.filters.length; i++) {
      if (
        this.state.filters[i][0] === category &&
        this.state.filters[i][1] === value
      ) {
        this.state.filters.splice(i, 1);
        this.paginateLogs(
          filterAllLogs(this.state.unfilteredLogs, this.state.filters)
        );
      }
    }
  }

  render() {
    return (
      <div className="App">
        <Sidebar />
        <div className="Body">
          <div className="Editor-breadcrumb">
            <h3> Poco Logging </h3>
          </div>
          <div id="Editor-body">
            {/* The left column of the Editor Page, consisting of areas to upload files and display them. */}
            <div id="left-column">
              <h1>Data</h1>
              <div style={{ paddingBottom: "2vh", width: "16vw" }}>
                <Select
                  options={this.state.services.map((s) => {
                    return { label: s, value: s };
                  })}
                  placeholder="Select a service"
                  onChange={(val, act) => this.updateService(val, act)}
                />
              </div>

              <div id="left-editors">
                <div className="Editor-column">
                  <div className="Text-background">
                    <div id="policy-text">
                      <TextArea
                        resize="none"
                        name="area"
                        isReadOnly
                        isCompact
                        defaultValue={this.state.json}
                        maxHeight="100%"
                      />
                    </div>
                  </div>
                  <div id="upload-div">
                    <input
                      type="file"
                      name="file"
                      accept=".json"
                      onChange={(event) => this.handleFileUpload(event, "json")}
                    />
                  </div>
                </div>
                <h1>Rego Policy</h1>
                <div className="Editor-column">
                  <div className="Text-background">
                    <div id="policy-text">
                      <TextArea
                        resize="none"
                        name="area"
                        isReadOnly
                        isCompact
                        defaultValue={this.state.rego}
                        maxHeight="100%"
                      />
                    </div>
                  </div>
                  <div id="upload-div">
                    <input
                      type="file"
                      name="file"
                      accept=".rego"
                      onChange={(event) => this.handleFileUpload(event, "rego")}
                    />
                  </div>
                </div>
              </div>
            </div>

            {/* The right column of the Editor Page, consisting of the logs in a Table Tree. */}

            <div id="right-column">
              <h1>Logs</h1>
              <div id="filter-project">
                <div style={{ width: "16vw" }}>
                  <Select
                    options={[
                      { label: "HTTP", value: "HTTP" },
                      { label: "Endpoint", value: "Endpoint" },
                      { label: "ID", value: "ID" },
                      { label: "Mechanism", value: "Mechanism" },
                      { label: "Principals", value: "Principals" },
                      { label: "Value", value: "Value" },
                      { label: "Header (Key)", value: "Header_Key" },
                      { label: "Header (Value)", value: "Header_Value" },
                    ]}
                    placeholder="Filter By"
                    onChange={(val, act) => this.updateFilter(val)}
                  />
                </div>

                <Textfield
                  onChange={(event) =>
                    this.setState({ filterText: event.target.value })
                  }
                  placeholder="Item"
                ></Textfield>

                <Button
                  appearance="default"
                  onClick={() =>
                    this.addFilter(this.state.filterBy, this.state.filterText)
                  }
                >
                  Add Filter
                </Button>
              </div>
              <div>
                <TagGroup>
                  {this.state.filters.map((key) => {
                    return this.generateTag(key[0], key[1]);
                  })}
                </TagGroup>
              </div>
              <div
                className="Text-background"
                style={{
                  height: this.state.filters.length === 0 ? "65%" : "61%",
                }}
              >
                <div className="Log-card">
                  {/* Top level of the table tree. Each Row here refers to an endpoint. */}
                  <TableTree>
                    {/* Defines Headers for our top table tree. */}
                    <div style={{ backgroundColor: "#f8f8f8" }}>
                      <Headers>
                        <Header width={135}>HTTP </Header>
                        <Header width={175}>Endpoint</Header>
                        <Header width={165}> </Header>
                        <Header width={250}> </Header>
                      </Headers>
                    </div>
                    {/* Rows of our top table tree. Each row refers to an endpoint. */}
                    <Rows
                      items={
                        this.state.paginatedInfo != null
                          ? this.state.paginatedInfo[this.state.pageNumber - 1]
                          : []
                      }
                      render={({ content, children }) =>
                        content.layer.toLowerCase() === "top"
                          ? generateEndpointRow(children, content)
                          : content.layer.toLowerCase() === "middle"
                          ? generateLogRow(children, content)
                          : content.layer.toLowerCase() === "bottom"
                          ? generateHeadersRow(children, content)
                          : generateTableHeaders(children, content)
                      }
                    />
                  </TableTree>
                </div>
              </div>
              <br></br>
              <div style={{ display: "flex", flexDirection: "row" }}>
                <Pagination
                  selectedIndex={this.state.pageNumber - 1}
                  pages={this.state.pages}
                  onChange={this.changeNum}
                />
                <div style={{ display: "flex", marginLeft: "auto" }}>
                  <Popup
                    isOpen={this.state.popupDisplayed}
                    onClose={() => this.setState({ popupDisplayed: false })}
                    placement="top-end"
                    content={() => userInstructions}
                    trigger={(triggerProps) => (
                      <Button
                        style={{ display: "flex", marginLeft: "auto" }}
                        {...triggerProps}
                        isSelected={this.state.popupDisplayed}
                        onClick={() => this.setState({ popupDisplayed: true })}
                        iconBefore={<QuestionCircleIcon label="Add" />}
                      />
                    )}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

import React from "react";
import { Row, Cell } from "@atlaskit/table-tree";
import InfoIcon from "@atlaskit/icon/glyph/info";
import { colors } from "@atlaskit/theme";

// Create the badge for the HTTP request type (POST, PUT, GET)
export function createHTTPBadge(httpRequestType) {
  var color;
  const GET = "#57D9A3";
  const PUT = "#FFC400";
  const POST = "#B5ECBA";

  switch (httpRequestType.toUpperCase()) {
    case "GET":
      color = GET;
      break;
    case "PUT":
      color = PUT;
      break;
    case "POST":
      color = POST;
      break;
    default:
      break;
  }
  return (
    <div className="HTTP-Button" style={{ backgroundColor: color }}>
      {httpRequestType}
    </div>
  );
}

// Generate an endpoint row (the topmost layer) display the path and HTTP badge.
export function generateEndpointRow(children, content) {
  return (
    <Row
      expandLabel="Expand"
      collapseLabel="Collapse"
      items={children}
      hasChildren={children != null && children.length > 0}
      isDefaultExpanded={true}
    >
      <Cell singleLine>{createHTTPBadge(content.http)}</Cell>
      <Cell style={{ fontSize: "15px", fontWeight: "500" }} singleLine>
        {content.endpoint}
      </Cell>
    </Row>
  );
}

// Generate our button displaying the diff, with appropriate green / red color choices.
export function generateDiffButton(text, changed) {
  var color;
  if (!changed) {
    color = "#DDDDDD"; // Light greyd
  } else if (text.toUpperCase() === "TRUE") {
    color = "#81F486"; // Pastel green color
  } else {
    color = "#FE9A9A";
  }
  return (
    <div className="Diff-button" style={{ backgroundColor: color }}>
      {text.toUpperCase()}
    </div>
  );
}

// Generate a row for the log.
export function generateLogRow(children, content) {
  var color = content.changed ? "#000000" : "#999999";
  return (
    <Row
      expandLabel="Expand"
      collapseLabel="Collapse"
      items={children}
      hasChildren={children != null && children.length > 0}
      isDefaultExpanded={false}
    >
      <Cell singleLine style={{ fontSize: "12px", color: color }}>
        {content.id}
      </Cell>
      <Cell singleLine style={{ fontSize: "12px", color: color }}>
        {content.mechanism}
      </Cell>
      <Cell singleLine style={{ fontSize: "12px", color: color }}>
        {content.principals}
      </Cell>
      <Cell>{generateDiffButton(content.value, content.changed)}</Cell>
      {/* <Cell>{content.changed ? "x" : <div />}</Cell> */}
    </Row>
  );
}

// Display a single header row, which consists of a key and value.
export function generateHeadersRow(children, content) {
  return (
    <Row>
      <Cell />
      <Cell singleLine style={{ fontSize: "12px" }}>
        {content.header_key}
      </Cell>
      <Cell singleLine style={{ fontSize: "12px" }}>
        {content.header_value}
      </Cell>
    </Row>
  );
}

// Display the headers in a different styling than the data.
export function generateTableHeaders(children, content) {
  if (content.layer.toLowerCase() === "header-bottom") {
    return (
      // TableHeaders for Headers
      <div style={{ backgroundColor: "#f8f8f8" }}>
        <Row style={{ backgroundColor: "red" }} className="Header-row">
          <Cell singleLine className="Table-header-container" />
          <Cell singleLine className="Table-header-container">
            <p>Headers</p>
          </Cell>
        </Row>
      </div>
    );
  } else {
    return (
      // TableHeaders for Logs
      <div style={{ backgroundColor: "#f8f8f8" }}>
        <Row className="Header-row">
          <Cell className="Table-header-container">
            <p>ID</p>
          </Cell>
          <Cell className="Table-header-container">
            <p>Mechanism</p>
          </Cell>
          <Cell className="Table-header-container">
            <p>Principals</p>
          </Cell>
          <Cell className="Table-header-container">
            <p>Value</p>
          </Cell>
        </Row>
      </div>
    );
  }
}

// Returns true if CONTENT[ATTRIBUTE] does not match EXPECTED.
export function shouldRemove(content, attribute, expected) {
  var value = content[attribute.toLowerCase()];

  if (!value) {
    return false;
  }

  return value.toLowerCase() !== expected.toLowerCase();
}

// move all endpoints that do not have logs to the bottom of the display
export function sortLogs(logs) {
  if (logs == null) {
    return [];
  }
  //count the number of endpoints without logs to prevent infinite refiltering
  var count = 0;
  for (var a = 0; a < logs.length - count; a += 1) {
    if (!logs[a].hasChildren) {
      logs.push(logs.splice(a, 1)[0]); //move endpoint to back
      a -= 1;
      count += 1;
    }
  }
  return logs;
}

// Run filterLogs() on LOGS for all items in FILTERS.
export function filterAllLogs(logs, filters) {
  for (var a = 0; a < filters.length; a += 1) {
    logs = filterLogs(logs, filters[a][0], filters[a][1]);
  }
  return sortLogs(logs);
}

// Returns a json based on LOGS, keeping only the logs where ATTRIBUTE has value VALUE.
export function filterLogs(logs, attribute, value) {
  // return if there is a null attribute or value
  if (value === "" || !attribute) {
    return logs;
  }

  logs = JSON.parse(JSON.stringify(logs));
  // For each endpoint
  for (var i = 0; i < logs.length; i++) {
    var endpoint = logs[i];

    // If endpoint attribute doesn't match value, remove it.
    if (shouldRemove(endpoint.content, attribute, value)) {
      logs.splice(i, 1);
      i--;
      continue;
    }

    if (!endpoint.hasChildren) continue;

    // For each log
    for (var j = 1; j < endpoint.children.length; j++) {
      var log = endpoint.children[j];

      // If log attribute doesn't match value, remove it.
      if (shouldRemove(log.content, attribute, value)) {
        endpoint.children.splice(j, 1);
        j--;
        continue;
      }

      if (!log.hasChildren) continue;

      // For each header
      for (var k = 1; k < log.children.length; k++) {
        var header = log.children[k];

        // If header attribute doesn't match value, remove it.
        if (shouldRemove(header.content, attribute, value)) {
          log.children.splice(k, 1);
          j--;
        }
      }

      // remove the header for headers if there are none left
      if (log.children.length === 1) {
        log.children = null;
        log.hasChildren = false;
      }
    }

    // remove the header for logs if there are none left for that endpoint
    if (endpoint.children.length === 1) {
      endpoint.children = null;
      endpoint.hasChildren = false;
    }
  }

  return logs;
}

export const userInstructions = (
  <div
    style={{
      width: "35vw",
      display: "flex",
      flexDirection: "row",
      padding: "10px",
      backgroundColor: colors.B50,
    }}
  >
    <div style={{ margin: "10px", paddingTop: "4px" }}>
      <InfoIcon primaryColor={colors.B300} size="medium" />
    </div>
    <div>
      <h2>Welcome to the POCO-Logger</h2>
      <p>On the left, upload your rego and/or json policy files.</p>
      <p>
        The table tree on the right will display the effects of these new
        policies on recent logs. The results are sorted by endpoint and can be
        expanded to view details about each individual log.
      </p>
      <p>
        Requests that are different from the last set of policy files will be
        display in a bright color.
      </p>
      <p>
        Requests that have not changed will be greyed out and sorted to the
        bottom.
      </p>
    </div>
  </div>
);

This contains an example of the log format. One thing to note is that, when written to S3, the files are gzipped to save storage space/cost. So they would need to be decompressed before being used.

You can "pretty-format" the `logs` file by doing `jq . logs` (make sure that you're in the right directory in the terminal). 

Here is a breakdown of some of the elements in the logs: 
Within the `ec2` block: 
* sn: service name, in the format of <service-name>-<build #>-<timestamp>
* hn: host name 
* id: ec2 instance id (not heavily used)
* az: availability zone 

Within the `m` (message) block: 
* sv: build ID 
* si: service name 
* di: message ID 

Within the `policyControl` block: 
The `decision_id` is unique to each decision but is currently not bveing heavily used. The open policy agent (OPA) does metrics by default, which is what we see in the `metrics` block. The query is always the same, and the result is the decision about whether or not to authorize. If the result value is true, then the person was authorized and able to make the request. `revision` is the version of the policy. 
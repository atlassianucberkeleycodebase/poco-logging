package platform

test = {
    "methods": {
        "Good_Subject": {"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "expected"}},
        "Bad_Subject": {"principals": ["external-service"], "method": "GET", "path": "/public/foo", "mechanism": "asap", "headers": {"X-SLAuth-Subject": "unexpected"}},
        "POST_method": {"principals": ["not-listed"], "method": "POST", "path": "/public/foo", "mechanism": "group"}
    }
}

test_combined_output {
  authorize == "true" with input as test.methods.Good_Subject
  authorize == "false" with input as test.methods.Bad_Subject
  authorize == "false" with input as test.methods.POST_method
}

test_available {
  available == "true"
}

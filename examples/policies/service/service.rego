package service

import input as req

enabled = true

deny["issuer requires certail subject"] {
  req.principals[_] == "external-service"
  not req.headers["X-SLAuth-Subject"] == "expected"
}

deny["issuer only allowed until date X"] {
  req.mechanism == "slauthtoken"
  req.principal == "temp-user"
  time.now_ns() > 10000000000000
}

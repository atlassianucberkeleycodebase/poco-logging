package platform
import data.service
import input as req

# Keywords / Constants
others = "_others"
expected_principals = [ p | p := req.principals[_]; service[req.mechanism][req.method][p] ]
principals = expected_principals {
    expected_principals[0]
} else = ["_others"] {
    is_other_principal
} else = [] {
  true
}

authorize = "true" {
    principal_output == set()
    method_output == set()
    path_output == set()
    custom_policy == set()
} else = "false" {
    true
}

# Healthcheck check to validate bundles are setup
available = "true" {
  is_object(service)
} else = "false" {
  true
}

# Base evaluations
principal_output = invalid_principals
method_output = invalid_method
path_output = invalid_path


# User defined custom policy
custom_policy = service.deny {
  is_object(service)
  service.enabled
  is_set(service.deny)
# deprecated name for custom policy interface
} else = service.authorize {
  is_object(service)
  service.enabled
  is_set(service.authorize)
} else = set() {
  true
}

# Top level query to wrap three queries into one
deny[result] {
    result := {"authorize": authorize, "principal": principal_output, "method": method_output, "path": path_output }
}

# For the principal listed under a mechanism, does the request's method match one of the whitelisted methods?
invalid_method["Method is not authorized"] {
  not service[req.mechanism][req.method]
}

# Is the principal explicitly whitelisted or is '_others' specified under mechanism as a catch-all?
invalid_principals["Principal is not whitelisted for authorization"] {
  not principals[0]
}


# For the principal listed under a mechanism, does the request's path match one of the whitelisted paths?
invalid_path["Path is not authorized"] {
  paths := [ path | p := principals[_]
           path := service[req.mechanism][req.method][p].paths[_]
           glob.match(path, ["/"], req.path)]
  not paths[0]
}

mechanism_has_others(mechanism, method) {
    service[mechanism][method][others]
}

is_other_principal {
    not expected_principals[0]
    mechanism_has_others(req.mechanism, req.method)
}

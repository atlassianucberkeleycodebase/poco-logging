# Example policies

## policy.rego

This is a shared policy distributed throughout the services for making the authorization decision.

## service/data.json

This is the normal file that the user would provide as their OPA data. Though we already have a CLI command that compliles it from something slightly nicer like:

```json
{
  "allow": [{
    "paths": ["/api/v1/**"],
    "methods": ["GET", "POST"],
    "principals": {
      "staff": {
        "groups": ["micros-sv--service-dl-admins"]
      },
      "asap": {
        "issuers": ["micros/service"]
      }
    }
  }, {
    "paths": ["/public/**"],
    "method": ["GET"],
    "principals": {
      "build": {
        "plans": ["*"]
      },
      "staff": {
        "groups": ["*"]
      },
      "asap": {
        "issuers": ["*"]
      }
    }
  }, {
    "paths": ["/admin/**"],
    "method": ["GET", "POST"],
    "principals": {
      "staff": {
        "groups": ["micros-sv--service-dl-admins"]
      }
    }
  }]
}
```

... I'll work on getting that code into the repo but for the time being the above is the format users would see and the data.json is what would be evaluated at runtime.

The data.json file is default deny with any matching rules allowing traffic as specified in the policy.rego.

## service/service.rego

Custom policy defined by the more 'advanced' users. This is a default allow policy which combines with the data.json to give more fine grained control over who can access what within the service.

# Running the policies

You can run the policy tests using the [OPA CLI](https://www.openpolicyagent.org/docs/latest/#running-opa).
```bash
$ tar -czf service.tar.gz service
$ opa test *.rego service.tar.gz
```

or play around with the policy using `opa run`
```bash
$ tar -czf service.tar.gz service
$  opa run *.rego service.tar.gz
Run 'help' to see a list of commands.

> data.service.deny with input as data.platform.test.methods.Bad_Subject
[
  "issuer requires certail subject"
]
```
